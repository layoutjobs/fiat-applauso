<?php

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$admin = dirname(dirname(dirname(__FILE__)));
$main  = dirname($admin);
Yii::setPathOfAlias('admin', $admin);
Yii::setPathOfAlias('ext', $admin . '/extensions');
Yii::setPathOfAlias('actions', $admin . '/actions');
Yii::setPathOfAlias('bootstrap', $admin . '/extensions/bootstrap');

return array(
	'basePath' => $main,
	'name' => 'Fiat Applàuso',
	
	'defaultController' => 'home',
    'controllerPath' => $admin . '/controllers',
    'viewPath' => $admin . '/views',
    'runtimePath' => $admin . '/runtime',
	 
	'language' => 'pt_br',
	
	// preloading components
	'preload' => array(
		'log',
		'bootstrap'
	),
	
	// autoloading model and component classes
	'import' => array(
        'admin.models.*',
        'admin.components.*',
		'admin.widgets.*',
		'application.models.*',
		'ext.mail.YiiMailMessage'
	),
	
	// application modules
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'admin',
			/*
			'generatorPaths' => array(
				'bootstrap.gii' // since 0.9.1
			)
			*/
		)
	),
	
	// application components
	'components' => array(
		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true
		),
		'coreMessages' => array(
			// necessário para o sistema ler os arquivos de mensagem na pasta /protected/messages.
			'basePath' => null
		),
		'user' => array(
			'loginUrl' => array('home/login'),
		),
		'clientScript' => array(
			'packages' => array(
				'googleMapsAPI' => array(
					'baseUrl' => 'http://maps.googleapis.com/maps/api',
					'js' => array(
						'js?sensor=false&language=' . Yii::app()->language . '&region=' . Yii::app()->language,
					)
				)
			)
		),

		// mysql connection
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=site2',
			'emulatePrepare' => true,
			'username' => 'layout',
			'password' => 'Yt7d2Ty5aAlJp4',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_'
		),
		'errorHandler' => array(
			'errorAction' => 'home/error'
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning'
				)
				
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class' => 'CWebLogRoute',
				),
				*/
			)
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.Bootstrap',
			'responsiveCss' => true
		),
		'mail' => array(
			/*
			'class' => 'ext.mail.YiiMail',
			'transportType' => 'smtp',
			'transportOptions' => array(
				'host' => '',
				'username' => '',
				'password' => '',
				'port' => '25',
				'encryption' => 'ssl'
			),
			*/
			'viewPath' => 'admin.views.mails',
			'logging' => true,
			'dryRun' => false
		)
	),
	
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => require($main . '/config/web/params.php')
);