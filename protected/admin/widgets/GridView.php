<?php

Yii::import('bootstrap.widgets.TbGridView');

class GridView extends TbGridView
{
	public $type = 'bordered striped';
	
	public $template = '{items}{pager}';
	
	public $pagerCssClass = 'pagination pagination-centered';
	
	public function init()
	{
		parent::init();
		if ($this->selectionChanged === null)
			$this->selectionChanged = "function(id){window.location='" . Yii::app()->controller->createUrl('update', array('id'=>'')) . "' + $.fn.yiiGridView.getSelection(id);}";
	}

}
