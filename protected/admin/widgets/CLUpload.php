<?php

class CLUpload extends CWidget
{
	/**
	 * @var array of options to be encoded and passed to the Fileupload Javascript API.
	 */
	public $options;
		
	/**
	 * @var string the url to the upload handler.
	 */
	public $url;
		
	/**
	 * @var string the form view to be rendered.
	 */
	public $formView = 'upload/form';
	
	/**
	 * @var string the upload view to be rendered.
	 */
	public $uploadView = 'upload/upload';
	
	/**
	 * @var string the download view to be rendered.
	 */
	public $downloadView = 'upload/download';
	
	/**
	 * @var string the gallery view to be rendered.
	 */
	public $galleryView = 'upload/gallery';	
	
	/**
	 * Publishes the required assets.
	 */
	public function init()
	{		
		parent::init();
		$this->publishAssets();
		
		$this->id = 'fileupload-' . $this->id;
	}
	
	/**
	 * Generates the required HTML and Javascript.
	 */
	public function run()
	{
		$options = $this->options ? CJavaScript::encode($this->options) : '[]';		
	
		$js = <<<EOD
$('#{$this->id}').fileupload();
$('#{$this->id}').fileupload('option', {$options});
$('#{$this->id}').each(function(){
    var that = this;
    $.getJSON(this.action, function(result) {
        if (result && result.length) {
            $(that).fileupload('option', 'done')
                .call(that, null, {result: result});
        }
    });
});	
EOD;

		Yii::app()->clientScript->registerScript($this->id, $js, CClientScript::POS_READY);
		
		if ($this->formView)
			$this->render($this->formView);			

		if ($this->uploadView)		
			$this->render($this->uploadView);

		if ($this->downloadView)		
			$this->render($this->downloadView);

		if ($this->galleryView)		
			$this->render($this->galleryView === null ? 'upload/gallery' : $this->galleryView);
	}
	
	/**
	 * Publises and registers the required CSS and Javascript.
	 * @throws CHttpException if the assets folder was not found.
	 */
	public function publishAssets()
	{
		$assets  = dirname(__FILE__) . '/assets/upload';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		
		if (is_dir($assets)) {
			// CSS to style the file input field as button and adjust the Bootstrap progress bars
			Yii::app()->clientScript->registerCssFile($baseUrl . '/css/jquery.fileupload-ui.css');

			// The jQuery UI widget factory, can be omitted if jQuery UI is already included
			Yii::app()->clientScript->registerScriptFile($baseUrl . "/js/vendor/jquery.ui.widget.js", CClientScript::POS_END);		

			// The Templates plugin is included to render the upload/download listings
			Yii::app()->clientScript->registerScriptFile($baseUrl . "/js/tmpl.min.js", CClientScript::POS_END);
			
			// The Load Image plugin is included for the preview images and image resizing functionality
			Yii::app()->clientScript->registerScriptFile($baseUrl . "/js/load-image.min.js", CClientScript::POS_END);
				
			// The Canvas to Blob plugin is included for image resizing functionality
			Yii::app()->clientScript->registerScriptFile($baseUrl . "/js/canvas-to-blob.min.js", CClientScript::POS_END);

			// Bootstrap Image Gallery
			Yii::app()->clientScript->registerScriptFile($baseUrl . "/js/bootstrap-image-gallery.min.js", CClientScript::POS_END);
			Yii::app()->clientScript->registerCssFile($baseUrl . "/css/bootstrap-image-gallery.min.css");

			// The Iframe Transport is required for browsers without support for XHR file uploads
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.iframe-transport.js', CClientScript::POS_END);
			
			// The basic File Upload plugin
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.fileupload.js', CClientScript::POS_END);

			// The File Upload file processing plugin
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.fileupload-fp.js', CClientScript::POS_END);
			
			//The File Upload user interface plugin
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.fileupload-ui.js', CClientScript::POS_END);
			
			// The localization script
			// Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/locale.js', CClientScript::POS_END);	
			
			/* <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
			<!--[if gte IE 8]><script src="<?php echo Yii::app()->baseUrl; ?>/js/cors/jquery.xdr-transport.js"></script><![endif]--> */
		} else {
			throw new CHttpException(500, __CLASS__ . ' - Error: Couldn\'t find assets to publish.');
		}
	}
}