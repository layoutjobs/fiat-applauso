<?php

class FuncionarioController extends Controller
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// allow admin user to perform all actions
			array(
				'allow',
				'users' => array('admin')
			),
			array(
				'allow',
				'actions' => array('login'),
				'users' => array('*')
			),
			array(
				'deny', // deny all users
				'users' => array('*')
			)
		);
	}
	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'upload' => array(
				'class' => 'actions.UploadAction',
				'path' => Funcionario::model()->getPathUpload(),
				'url' => Funcionario::model()->getUrlUpload(),
				'options' => array(
					'accept_file_types' => '/(\.|\/)(gif|jpe?g|png)$/i',
					'image_versions' => array(
						'large' => array(
							'max_width' => 800,
							'max_height' => 800,
							'jpeg_quality' => 95
						),
						'normal' => array(
							'max_width' => 350,
							'max_height' => 350,
							'jpeg_quality' => 95
						),
						'thumbnail' => array(
							'max_width' => 150,
							'max_height' => 150,
						)
					)
				)
			)
		);
	}
	
	/**
	 * Creates a new model.
	 */
	public function actionCreate()
	{
		$model = new Funcionario;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if (isset($_POST['Funcionario'])) {
			$model->attributes = $_POST['Funcionario'];
			if ($model->save()) {
				Yii::app()->user->setFlash('sucesso', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->redirect(array(
					'update',
					'id' => $model->id
				));
			}
		}
		
		$this->render('create', array(
			'model' => $model
		));
	}
	
	/**
	 * Updates a particular model.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if (isset($_POST['Funcionario'])) {
			$model->attributes = $_POST['Funcionario'];
			if ($model->save()) {
				Yii::app()->user->setFlash('sucesso', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->refresh();
			}
		}
		
		$this->render('update', array(
			'model' => $model
		));
	}
	
	/**
	 * Action de upload de imagens.
	 */
	public function actionUploadImgs($id)
	{
		$model = $this->loadModel($id);
		
		$this->render('uploadImgs', array(
			'model' => $model
		));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();
			
			// if AJAX request (triggered by deletion via admin grid view), we should not
			// redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array(
					'index'
				));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}
	
	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Funcionario('search');
		$model->unsetAttributes();
		// clear any default values
		if (isset($_GET['Funcionario']))
			$model->attributes = $_GET['Funcionario'];
		
		$this->render('index', array(
			'model' => $model
		));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = Funcionario::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'funcionario-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}