<?php

class ConfigController extends Controller 
{
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array(
					'index',
				),
				'users' => array(
					'admin'
				)
			),
			array(
				'deny', // deny all users
				'users' => array(
					'*'
				)
			)
		);
	}
	
	public function actionIndex() 
	{
		$file    = dirname(__FILE__) . '../../../data/params.inc';
		$content = file_get_contents($file);
		$arr     = unserialize(base64_decode($content));
		$model   = new ConfigForm();
		$model->setAttributes($arr);
		
		if (isset($_POST['ConfigForm'])) {
			$model->setAttributes($_POST['ConfigForm']);
			
			if (CUploadedFile::getInstance($model, 'bannerHome1'))
				$model->bannerHome1 = CUploadedFile::getInstance($model, 'bannerHome1');
			
			if (CUploadedFile::getInstance($model, 'bannerHome2'))
				$model->bannerHome2 = CUploadedFile::getInstance($model, 'bannerHome2');
			
			if (CUploadedFile::getInstance($model, 'bannerHome3'))
				$model->bannerHome3 = CUploadedFile::getInstance($model, 'bannerHome3');
			
			if ($model->validate()) {
				$config = $_POST['ConfigForm'];
				
				if (CUploadedFile::getInstance($model, 'bannerHome1')) 
					$model->bannerHome1->saveAs(Yii::app()->getBasePath() . '/../images/banners/home-index-1.png');

				if (CUploadedFile::getInstance($model, 'bannerHome2'))
					$model->bannerHome2->saveAs(Yii::app()->getBasePath() . '/../images/banners/home-index-2.png');
					
				if (CUploadedFile::getInstance($model, 'bannerHome3'))
					$model->bannerHome3->saveAs(Yii::app()->getBasePath() . '/../images/banners/home-index-3.png');

				$str = base64_encode(serialize($config));
				file_put_contents($file, $str);
				Yii::app()->user->setFlash('sucesso', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
			}
		}

		$this -> render('index', array(
			'model' => $model
		));
	}

}
