<?php
$this->toolbar = array(
	array(
		'label' => 'Voltar',
		'icon' => 'arrow-left',
		'htmlOptions' => array('rel' => 'tooltip', 'title' => 'Voltar para Todos os Registros'),
		'url' => array('index')
	)
);

$this->pageTitle = 'Informações';
$this->pageSubtitle = 'Incluindo uma nova Informação';
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>