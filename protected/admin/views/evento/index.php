<?php
$this->toolbar = array(
	array(
		'type' => 'inverse',
		'label' => 'Incluir',
		'icon' => 'plus white',
		'url' => array('create')
	)
);

$this->pageTitle = 'Informações';
$this->pageSubtitle = 'Listagem de todas as Informações';
?>

<?php
$this->widget('GridView', array(
	'id' => 'evento-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'type' => 'html',
			'value' => 'CHtml::image($data->getUrlImgDestaque(\'thumbnails\'))',
			'htmlOptions'=>array('class'=>'img'),
		),
		array(
			'name' => 'ativo',
			'value' => '($data->ativo==="1")?"Sim":"Não"',
			'filter' => '',
			'cssClassExpression' => '($data->ativo==="1")?"check":"uncheck"',
		),
		array(
			'name' => 'id',
			'value' => '$data->id',
			'htmlOptions'=>array('class'=>'id'),
		),
		array(
			'name' => 'data',
			'value' => '$data->data',
			'filter' => '',
			'htmlOptions'=>array('class'=>'data'),
		),
		array(
			'name' => 'tipo',
			'value' => '$data->getTipo()',
			'filter' => $model->getTipoList(),
		),
		'titulo',
		'subtitulo',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '<div class="btn-toolbar">{update}{delete}</div>',
			'deleteConfirmation' => "js:'Tem certeza que deseja excluir este registro?'",
			'buttons' => array(
				'update' => array('options' => array('class' => 'btn btn-mini editar')),
				'delete' => array('options' => array('class' => 'btn btn-mini excluir')),
			),
		),
	),
));
?>