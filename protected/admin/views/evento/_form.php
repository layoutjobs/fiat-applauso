<div class="form">
	<?php 
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'evento-form',
		'type' => 'horizontal',
	)); 
	?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="span7">
			<fieldset>
				<?php echo $form->dropDownListRow($model, 'tipo', $model->getTipoList(), array('class' => 'span4', 'empty' => '')); ?>
				<?php echo $form->textFieldRow($model, 'titulo', array('class' => 'span4')); ?>
				<?php echo $form->textFieldRow($model, 'subtitulo', array('class' => 'span4')); ?>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'data', array('class' => 'control-label')); ?>
					<div class="controls">
						<?php 
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'language' => 'pt-BR',
							'model' => $model,
							'attribute' => 'data',
							'htmlOptions' => array('class' => 'span4'),
						));
						?>
					</div>
				</div>
				<?php echo $form->textAreaRow($model, 'conteudo', array('rows' => '15', 'class' => 'span4')); ?>
				<?php echo $form->hiddenField($model, 'imgDestaque'); ?>
			</fieldset>
		</div>
		<div class="span5">
			<fieldset class="well">
				<div class="control-group">
					<?php echo $form->labelEx($model, 'ativo', array('class' => 'control-label')); ?>
					<div class="controls">
						<label class="checkbox">
							<?php echo $form->checkBox($model, 'ativo'); ?>
							Sim, ativar este registro.
						</label>
					</div>
				</div>
			</fieldset>

			<?php if(!$model->isNewRecord) : ?>
			<fieldset>
				<legend>Imagem de destaque</legend>
				<p>Clique sobre uma imagem para defini-la como destaque.</p>
				<ul class="thumbnails">
					<?php foreach($model->getImgs('thumbnails') as $img) : ?>
					<li class="span1">
						<a class="thumbnail<?php echo ($img === 'thumbnails/' . $model->imgDestaque || $img === $model->imgDestaque)  ? ' active' : ''; ?>" data-content="<?php echo $img; ?>" href="#">
							<?php echo CHtml::image($model->getUrlUpload() . $model->pastaImgs . '/' . $img); ?>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</fieldset>
			<?php endif; ?>
		</div>
	</div>
	<div class="form-actions">
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type' => 'inverse',
			'icon' => 'ok white',
			'label' => 'Salvar',
		)); 
		?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div>

<?php Yii::app()->clientScript->registerScript('evento-form', "
$('#evento-form .thumbnails a').click(function(e){
	$('#Evento_imgDestaque').attr('value',$(this).attr('data-content'));
	$('#evento-form .thumbnail').removeClass('active');
	$(this).addClass('active');
	e.preventDefault();
});
");
?>