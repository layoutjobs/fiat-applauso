<?php
$this->toolbar = array(
	array(
		'buttons' => array(
			array(
				'label' => 'Voltar',
				'icon' => 'arrow-left',
				'htmlOptions' => array('rel' => 'tooltip', 'title' => 'Voltar para Todos os Registros'),
				'url' => array('index')
			)
		)
	),
	array(
		'buttons' => array(
			array(
				'label' => 'Upload de imagens',
				'icon' => 'picture',
				'url' => array('uploadImgs', 'id' => $model->id)
			)
		)
	),
	array(
		'type' => 'danger',
		'buttons' => array(
			array(
				'label' => 'Excluir',
				'icon' => 'trash white',
				'url' => '#',
				'htmlOptions' => array(
					'submit' => array('delete', 'id' => $model->id),
					'confirm' => 'Tem certeza que deseja excluir este registro?'
				)
			)
		)
	)
);

$this->pageTitle = 'Informações';
$this->pageSubtitle = 'Editando a Informação #' . $model->id;
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>