<?php 
$this->pageTitle = 'Login';
$this->pageSubtitle = 'Informe seus dados para acessar o sistema';
?>

<div class="span4 pull-center">
	<div class="login-form">
		<?php 
		$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'home-login-form',
			'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
		)); 
		?>
		
		<?php echo $form->errorSummary($model, ''); ?>
	
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model, 'username', array('class' => 'span3')); ?>
	
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model, 'password', array('class' => 'span3')); ?>
	
	 	<label class="checkbox">
	    	<?php echo $form->checkBox($model, 'rememberMe'); ?>
	    	<?php echo $model->getAttributeLabel('rememberMe'); ?>
		</label>
		
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'label' => 'Login'
		)); 
		?>
	
		<?php $this->endWidget(); ?>
	</div><!-- form -->
</div>
