<?php
$this->toolbar = array(
	array(
		'type' => 'inverse',
		'label' => 'Incluir',
		'icon' => 'plus white',
		'url' => array('create')
	)
);

$this->pageTitle = 'Produtos';
$this->pageSubtitle = 'Listagem de todos os Produtos';
?>

<?php
$this->widget('GridView', array(
	'id' => 'produto-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'type' => 'html',
			'value' => 'CHtml::image($data->getUrlImgDestaque(\'thumbnails\'))',
			'htmlOptions'=>array('class'=>'img'),
		),
		array(
			'name' => 'ativo',
			'value' => '($data->ativo==="1")?"Sim":"Não"',
			'filter' => '',
			'cssClassExpression' => '($data->ativo==="1")?"check":"uncheck"',
		),
		array(
			'name' => 'id',
			'value' => '$data->id',
			'htmlOptions'=>array('class'=>'id'),
		),
		'nome',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '<div class="btn-toolbar">{update}{delete}</div>',
			'deleteConfirmation' => "js:'Tem certeza que deseja excluir este registro?'",
			'buttons' => array(
				'update' => array('options' => array('class' => 'btn btn-mini editar')),
				'delete' => array('options' => array('class' => 'btn btn-mini excluir')),
			),
		),
	),
));
?>