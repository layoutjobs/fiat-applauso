<?php
$this->toolbar = array(
	array(
		'type' => 'inverse',
		'label' => 'Incluir',
		'icon' => 'plus white',
		'url' => array('create')
	)
);

$this->pageTitle = 'Veículos';
$this->pageSubtitle = 'Listagem de todos os Veículos';
?>

<?php
$this->widget('GridView', array(
	'id' => 'veiculo-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'type' => 'html',
			'value' => 'CHtml::image($data->getUrlImgDestaque(\'thumbnails\'))',
			'htmlOptions'=>array('class'=>'img'),
		),
		array(
			'name' => 'ativo',
			'value' => '($data->ativo==="1")?"Sim":"Não"',
			'filter' => '',
			'cssClassExpression' => '($data->ativo==="1")?"check":"uncheck"',
		),
		'placa',
		'chassi',
		'nome',
		array(
			'name' => 'marca',
			'value' => '$data->marca',
			'filter' => $model->getMarcaList(),
		),
		'modelo',
		/*
		array(
			'name' => 'anoModelo',
			'value' => '$data->anoModelo',
			'filter' => $model->getAnosList(),
			'htmlOptions'=>array('class' => 'data'),
		),
		array(
			'name' => 'anoLancto',
			'value' => '$data->anoLancto',
			'filter' => $model->getAnosList(),
			'htmlOptions'=>array('class' => 'data'),
		),
		*/
		array(
			'name' => 'novo',
			'value' => '($data->novo==="1")?"Sim":"Não"',
			'filter' => '',
			'cssClassExpression' => '($data->novo==="1")?"check":"uncheck"',
		),
		array(
			'name' => 'prontaEntrega',
			'value' => '($data->prontaEntrega==="1")?"Sim":"Não"',
			'filter' => '',
			'cssClassExpression' => '($data->prontaEntrega==="1")?"check":"uncheck"',
		),
		array(
			'name' => 'promocao',
			'value' => '($data->promocao==="1")?"Sim":"Não"',
			'filter' => '',
			'cssClassExpression' => '($data->promocao==="1")?"check":"uncheck"',
		),
		array(
			'name' => 'filtro',
			'value' => '($data->promocao==="1")?"Sim":"Não"',
			'filter' => '',
			'cssClassExpression' => '($data->filtro==="1")?"check":"uncheck"',
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '<div class="btn-toolbar">{update}{delete}</div>',
			'deleteConfirmation' => "js:'Tem certeza que deseja excluir este registro?'",
			'buttons' => array(
				'update' => array('options' => array('class' => 'btn btn-mini editar')),
				'delete' => array('options' => array('class' => 'btn btn-mini excluir')),
			),
		),
	),
));
?>