<div class="form">
	<?php 
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'veiculo-form',
		'type' => 'horizontal',
	)); 
	?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="span7">
			<fieldset>
				<?php echo $form->dropDownListRow($model, 'marca', $model->getMarcaList(), array('class' => 'span4', 'empty' => '')); ?>
				<?php echo $form->textFieldRow($model, 'nome', array('class' => 'span4')); ?>
				<?php echo $form->textFieldRow($model, 'modelo', array('class' => 'span4')); ?>
				<?php echo $form->dropDownListRow($model, 'anoModelo', $model->getAnosList(), array('class' => 'span4', 'empty' => '')); ?>
				<?php echo $form->dropDownListRow($model, 'anoLancto', $model->getAnosList(), array('class' => 'span4', 'empty' => '')); ?>
				<?php echo $form->textFieldRow($model, 'kmRodado', array('class' => 'span4')); ?>
				<?php echo $form->dropDownListRow($model, 'qtdePortas', $model->getQtdePortasList(), array('class' => 'span4', 'empty' => '')); ?>
				<?php echo $form->dropDownListRow($model, 'combustivel', $model->getCombustivelList(), array('class' => 'span4', 'empty' => '')); ?>
				<?php echo $form->textFieldRow($model, 'cor', array('class' => 'span4')); ?>
				<?php echo $form->textAreaRow($model, 'acessorios', array('class' => 'span4', 'rows' => '6')); ?>
				<?php echo $form->textFieldRow($model, 'placa', array('class' => 'span4')); ?>
				<?php echo $form->textFieldRow($model, 'chassi', array('class' => 'span4')); ?>
				<?php echo $form->textFieldRow($model, 'valor', array('class' => 'span4')); ?>
				<?php echo $form->hiddenField($model, 'imgDestaque', array('class' => 'span4')); ?>
			</fieldset>
		</div>
		<div class="span5">
			<fieldset class="well">
				<div class="control-group">
					<?php echo $form->labelEx($model, 'ativo', array('class' => 'control-label')); ?>
					<div class="controls">
						<label class="checkbox">
							<?php echo $form->checkBox($model, 'ativo'); ?>
							Sim, ativar este registro.
						</label>
					</div>
				</div>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'prontaEntrega', array('class' => 'control-label')); ?>
					<div class="controls">
						<label class="checkbox">
							<?php echo $form->checkBox($model, 'prontaEntrega'); ?>
							Sim, veículo a pronta-entrega (Novo).
						</label>
					</div>
				</div>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'promocao', array('class' => 'control-label')); ?>
					<div class="controls">
						<label class="checkbox">
							<?php echo $form->checkBox($model, 'promocao'); ?>
							Sim, veículo em promoção.
						</label>
					</div>
				</div>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'filtro', array('class' => 'control-label')); ?>
					<div class="controls">
						<label class="checkbox">
							<?php echo $form->checkBox($model, 'filtro'); ?>
							Sim, este é um filtro para os demais veículos com as mesmas características.
						</label>
					</div>
				</div>
			</fieldset>

			<?php if(!$model->isNewRecord) : ?>
			<fieldset>
				<legend>Imagem de destaque</legend>
				<p>Clique sobre uma imagem para defini-la como destaque.</p>
				<ul class="thumbnails">
					<?php foreach($model->getImgs('thumbnails') as $img) : ?>
					<li class="span1">
						<a class="thumbnail<?php echo ($img === 'thumbnails/' . $model->imgDestaque || $img === $model->imgDestaque)  ? ' active' : ''; ?>" data-content="<?php echo $img; ?>" href="#">
							<?php echo CHtml::image($model->getUrlUpload() . $model->pastaImgs . '/' . $img); ?>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</fieldset>
			<?php endif; ?>
		</div>
	</div>
	<div class="form-actions">
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type' => 'inverse',
			'icon' => 'ok white',
			'label' => 'Salvar',
		)); 
		?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div>

<?php Yii::app()->clientScript->registerScript('veiculo-form', "
$('#veiculo-form .thumbnails a').click(function(e){
	$('#Veiculo_imgDestaque').attr('value',$(this).attr('data-content'));
	$('#veiculo-form .thumbnail').removeClass('active');
	$(this).addClass('active');
	e.preventDefault();
});
");
?>