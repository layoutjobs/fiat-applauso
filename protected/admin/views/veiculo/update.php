<?php
$this->toolbar = array(
	array(
		'label' => 'Voltar',
		'icon' => 'arrow-left',
		'htmlOptions' => array('rel' => 'tooltip', 'title' => 'Voltar para Todos os Registros'),
		'url' => array('index')
	),
	array(
		'label' => 'Upload de imagens',
		'icon' => 'picture',
		'url' => array('uploadImgs', 'id' => $model->id)
	),
	array(
		'type' => 'danger',
		'label' => 'Excluir',
		'icon' => 'trash white',
		'url' => '#',
		'htmlOptions' => array(
			'submit' => array('delete', 'id' => $model->id),
			'confirm' => 'Tem certeza que deseja excluir este registro?'
		)
	)
);
	
$this->pageTitle = 'Veículos';
$this->pageSubtitle = 'Editando o Veículo #' . $model->id;
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>