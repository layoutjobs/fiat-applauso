<?php
$this->toolbar = array(
	array(
		'label' => 'Voltar',
		'icon' => 'arrow-left',
		'htmlOptions' => array('rel' => 'tooltip', 'title' => 'Voltar para Todos os Registros'),
		'url' => array('index')
	)
);
	
$this->pageTitle = 'Veículos';
$this->pageSubtitle = 'Incluindo um novo Veículo';
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>