<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="pt-BR" />
    
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/admin/main.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/admin/responsive.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/tools.css'); ?>
	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<title><?php echo Yii::app()->name; ?> | <?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body class="<?php echo $this->id . ' ' . $this->action->id; ?>">
	
<?php
if (!Yii::app()->user->isGuest) {
	$this->widget('bootstrap.widgets.TbNavbar', array(
		'fixed' => 'top',
		'brand' => CHtml::encode(Yii::app()->name),
		'brandUrl' => Yii::app()->createUrl('home/index'),
		'collapse' => true,
		'type' => 'inverse',
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'items' => array(
					array(
						'label' => 'Veículos',
						'url' => array('/veiculo/index')
					),
					array(
						'label' => 'Informações',
						'url' => array('/evento/index')
					),
					array(
						'label' => 'Funcionários',
						'url' => array('/funcionario/index')
					),
					array(
						'label' => 'Produtos',
						'url' => array('/produto/index')
					),
					array(
						'label' => 'Configurações',
						'url' => array('/config/index')
					)
				)
			),
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'htmlOptions' => array(
					'class' => 'pull-right'
				),
				'items' => array(
					array(
						'label' => 'Ir para o site',
						'url' => Yii::app()->baseUrl . '/'
					),
					/*
					array(
						'label' => 'Login',
						'url' => array('/home/login'),
						'visible' => Yii::app()->user->isGuest
					),
					*/
					array(
						'label' => 'Sair (' . Yii::app()->user->name . ')',
						'url' => array('/home/logout'),
						//'visible' => !Yii::app()->user->isGuest
					)
				)
			)
		)
	));
}
?>	

<section class="container" id="page">
	<div class="page-header">
		<h1><?php echo $this->pageTitle; ?> <small><?php echo $this->pageSubtitle; ?></small></h1>
	</div>

	<div id="toolbar">
		<div class="btn-toolbar">	
			<?php foreach($this->toolbar as $item) : ?>	
			<?php if (array_key_exists('buttons', $item)) : ?>
				<?php $this->widget('bootstrap.widgets.TbButtonGroup', $item); ?>	
			<?php else : ?>
			<div class="btn-group">
				<?php $this->widget('bootstrap.widgets.TbButton', $item); ?>
			</div>
			<?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div><!-- toolbar -->
	
	<?php if(Yii::app()->user->hasFlash('sucesso')): ?>
	<p class="flash alert alert-success text-center"><?php echo Yii::app()->user->getFlash('sucesso'); ?></p>
	<?php endif; ?>
	
	<?php if(Yii::app()->user->hasFlash('erro')): ?>
	<p class="flash alert alert-error text-center"><?php echo Yii::app()->user->getFlash('erro'); ?></p>
	<?php endif; ?>

	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</section><!-- page -->

<footer id="footer">
	<p>Copyright &copy; <?php echo date('Y'); ?>. <?php echo CHtml::encode(Yii::app()->name); ?>. Todos os direitos reservados.</p>
</footer><!-- footer -->

<div id="ajax-loading"></div>

<?php
Yii::app()->clientScript->registerScript('layout', '
jQuery(\'#ajax-loading\').bind(\'ajaxSend\',function(){
	$(\'body\').addClass(\'loading\');
}).bind(\'ajaxComplete\',function(){
	$(\'body\').removeClass(\'loading\');
});
');
?>

<!--[if IE]>
	<link href="<?php echo Yii::app()->baseUrl; ?>/admin/css/ie.css" rel="stylesheet" />
<![endif]-->
</body>
</html>
