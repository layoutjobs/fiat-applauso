<?php
$this->pageTitle = 'Configurações';
$this->pageSubtitle = 'Definições de parâmetros do sistema';
?>

<div class="form">
 
	<?php 
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'config-form',
		'type' => 'horizontal',
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	)); 
	?>
 
    <?php echo $form->errorSummary($model); ?>
 
	<div class="row">
		<div class="span6">
			<fieldset>	
				<legend>Empresa</legend>
 				<?php echo $form->textFieldRow($model, 'foneContato', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'endereco', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'linkFacebook', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'mapaLat', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'mapaLng', array('class' => 'span4')); ?>
 			</fieldset>
 			<fieldset>
 				<legend>E-mails</legend>
 				<?php echo $form->textFieldRow($model, 'emailContato', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'emailNewsletter', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'emailTestDrive', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'emailRevisao', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'emailAcessorio', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'emailFrotista', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'emailFashion', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'emailTrabalheConosco', array('class' => 'span4')); ?>
			</fieldset>
			<fieldset>	
				<legend>Trabalhe conosco</legend>
 				<?php echo $form->textAreaRow($model, 'cargos', array('class' => 'span4')); ?>
 				<?php echo $form->textAreaRow($model, 'grausInstrucao', array('class' => 'span4')); ?>
			</fieldset>
		</div>
		<div class="span6">
			<fieldset>	
				<legend>Veículos</legend>
 				<?php echo $form->textAreaRow($model, 'marcasVeiculo', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'qtdesPortasVeiculo', array('class' => 'span4')); ?>
 				<?php echo $form->textAreaRow($model, 'combustiveisVeiculo', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'anoDeVeiculo', array('class' => 'span4')); ?>
 				<?php echo $form->textFieldRow($model, 'anoAteVeiculo', array('class' => 'span4')); ?>
			</fieldset>
			<fieldset>	
				<legend>Meta tags</legend>
 				<?php echo $form->textFieldRow($model, 'metaTitle', array('class' => 'span4')); ?>
 				<?php echo $form->textAreaRow($model, 'metaKeywords', array('class' => 'span4')); ?>
 				<?php echo $form->textAreaRow($model, 'metaDescription', array('class' => 'span4')); ?>
			</fieldset>
			<fieldset>	
				<legend>Banners</legend>
				<p>Para uma melhor visualização no site, utilize imagens com 940px x 400px.
				Somente extensão .PNG é aceita.</p>
				<br />
				<?php echo $form->fileFieldRow($model, 'bannerHome1', array(
					'hint' => is_file(Yii::app()->basePath . '/../images/banners/home-index-1.png') ? '<i class="icon icon-ok"></i> Arquivo OK.' : '<i class="icon icon-remove"></i> Nenhum arquivo.'
				)); ?>
				<?php echo $form->fileFieldRow($model, 'bannerHome2', array(
					'hint' => is_file(Yii::app()->basePath . '/../images/banners/home-index-2.png') ? '<i class="icon icon-ok"></i> Arquivo OK.' : '<i class="icon icon-remove"></i> Nenhum arquivo.'
				)); ?>
				<?php echo $form->fileFieldRow($model, 'bannerHome3', array(
					'hint' => is_file(Yii::app()->basePath . '/../images/banners/home-index-3.png') ? '<i class="icon icon-ok"></i> Arquivo OK.' : '<i class="icon icon-remove"></i> Nenhum arquivo.'
				)); ?>
			</fieldset>
		</div>
	</div> 

	<div class="form-actions">
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type' => 'inverse',
			'icon' => 'ok white',
			'label' => 'Salvar',
		)); 
		?>
	</div>
 
	<?php $this->endWidget(); ?>
</div><!-- form -->