<?php
$this->toolbar = array(
	array(
		'type' => 'inverse',
		'icon' => 'ok white',
		'url' => array('update', 'id' => $model->id),
		'label' => 'Concluído'
	)
);
	
$this->pageTitle = 'Funcionários';
$this->pageSubtitle = 'Upload de images do Funcionário #' . $model->id;
?>

<p>As imagens deste funcionário são armazenadas na pasta
<span class="label"><?php echo CHtml::encode($model->pastaImgs); ?></span>.<br />
<strong>Importante:</strong> Evite utilizar imagens com espaços, acentos ou caracteres 
especiais (Ç, !, ?, etc.), assim como nomes muito longos (máximo 128 caracteres).</p> 	

<?php $this->widget('CLUpload', array(
	'url' => $this->createUrl('upload', array('folder' => $model->pastaImgs)),
	'galleryView' => false
)); ?>