<div class="form">
	<?php 
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'funcionario-form',
		'type' => 'horizontal',
	)); 
	?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="span7">
			<fieldset>
				<?php echo $form->textFieldRow($model, 'nome', array('class' => 'span4')); ?>
				<?php echo $form->dropDownListRow($model, 'equipe', $model->getEquipeList(), array('class' => 'span4', 'empty' => '')); ?>
				<?php echo $form->textFieldRow($model, 'telefone', array('class' => 'span4')); ?>
				<?php echo $form->textFieldRow($model, 'ramal', array('class' => 'span4')); ?>
				<?php echo $form->textFieldRow($model, 'celular', array('class' => 'span4')); ?>
				<?php echo $form->textFieldRow($model, 'email', array('class' => 'span4')); ?>
				<?php echo $form->textAreaRow($model, 'perfil', array('rows' => '5', 'class' => 'span4')); ?>
				<?php echo $form->hiddenField($model, 'imgDestaque', array('class' => 'span4')); ?>
			</fieldset>
		</div>
		<div class="span5">
			<fieldset class="well">
				<div class="control-group">
					<?php echo $form->labelEx($model, 'ativo', array('class' => 'control-label')); ?>
					<div class="controls">
						<label class="checkbox">
							<?php echo $form->checkBox($model, 'ativo'); ?>
							Sim, ativar este registro.
						</label>
					</div>
				</div>
			</fieldset>
			
			<?php if(!$model->isNewRecord) : ?>
			<fieldset>
				<legend>Imagem de destaque</legend>
				<p>Clique sobre uma imagem para defini-la como destaque.</p>
				<ul class="thumbnails">
					<?php foreach($model->getImgs('thumbnails') as $img) : ?>
					<li class="span1">
						<a class="thumbnail<?php echo ($img === 'thumbnails/' . $model->imgDestaque || $img === $model->imgDestaque)  ? ' active' : ''; ?>" data-content="<?php echo $img; ?>" href="#">
							<?php echo CHtml::image($model->getUrlUpload() . $model->pastaImgs . '/' . $img); ?>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</fieldset>
			<?php endif; ?>
		</div>
	</div>
	<div class="form-actions">
		<?php 
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type' => 'inverse',
				'icon' => 'ok white',
				'label' => 'Salvar',
			)); 
		?>
	</div>
	
	<?php $this->endWidget(); ?>
	
</div>

<?php Yii::app()->clientScript->registerScript('funcionario-form', "
$('#funcionario-form .thumbnails a').click(function(e){
	$('#Funcionario_imgDestaque').attr('value',$(this).attr('data-content'));
	$('#funcionario-form .thumbnail').removeClass('active');
	$(this).addClass('active');
	e.preventDefault();
});
"); ?>