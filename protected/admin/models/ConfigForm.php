<?php

class ConfigForm extends CFormModel 
{
	public $foneContato;
	public $endereco;
	public $linkFacebook;
	public $mapaLat;
	public $mapaLng;
	public $emailNewsletter;
	public $emailContato;
	public $emailTestDrive;
	public $emailRevisao;
	public $emailAcessorio;
	public $emailFrotista;
	public $emailFashion;	
	public $emailTrabalheConosco;
	public $marcasVeiculo;
	public $qtdesPortasVeiculo;
	public $combustiveisVeiculo;
	public $anoDeVeiculo;
	public $anoAteVeiculo;
	public $metaTitle;
	public $metaKeywords;
	public $metaDescription;
	public $cargos;
	public $grausInstrucao;
	public $bannerHome1;
	public $bannerHome2;
	public $bannerHome3;
	
	public function rules() 
	{
		return array( 
			array(
				'foneContato, endereco, linkFacebook, mapaLat, mapaLng, emailNewsletter, emailContato, 
				 emailTestDrive, emailRevisao, emailAcessorio, emailFrotista, emailFashion, emailTrabalheConosco,
				 marcasVeiculo, qtdesPortasVeiculo, combustiveisVeiculo, anoDeVeiculo, anoAteVeiculo, 
				 metaTitle, metaKeywords, metaDescription, cargos, grausInstrucao', 
				 'required'
			), 
			array(
				'linkFacebook', 
				'url'
			),
			array(
				'anoDeVeiculo, anoAteVeiculo', 
				'length',
				'min' => 4,
			),
			array(
				'anoDeVeiculo, anoAteVeiculo', 
				'length',
				'max' => 4,
			),
			array(
				'anoDeVeiculo, anoAteVeiculo', 				
				'numerical',
				'integerOnly' => true
			),
			array(
				'bannerHome1, bannerHome2, bannerHome3', 
				'file', 
				'types' => 'png',
				'allowEmpty' => true,
			),
			array(
				'bannerHome1, bannerHome2, bannerHome3', 
				'safe'
			),
			/*
			array(
				'emailNewsletter, emailContato, emailTestDrive, emailRevisao, emailAcessorio, emailFrotista', 
				'email'
			)
			*/
		);
	}

	public function attributeLabels()
	{
		return array(
			'foneContato' => 'Telefone',
			'endereco' => 'Endereço',
			'linkFacebook' => 'Link Facebook',
			'mapaLat' => 'Latitude do mapa',
			'mapaLng' => 'Longitude do mapa',
			'emailNewsletter' => 'Newsletters',
			'emailContato' => 'Contato (Principal)',	
			'emailTestDrive' => 'Teste drive',	
			'emailRevisao' => 'Revisão',	
			'emailAcessorio' => 'Acessórios',	
			'emailFrotista' => 'Frotistas',
			'emailFashion' => 'Fiat Fashion',
			'emailTrabalheConosco' => 'Trabalhe Conosco',
			'marcasVeiculo' => 'Marcas',
			'qtdesPortasVeiculo' => 'Portas',	
			'combustiveisVeiculo' => 'Combustíveis',
			'anoDeVeiculo' => 'Ano de',
			'anoAteVeiculo' => 'Ano até',
			'metaTitle' => 'Título do site',
			'metaDescription' => 'Descrição do site',
			'metaKeywords' => 'Palavras chave',
			'cargos' => 'Cargos',	
			'grausInstrucao' => 'Graus de Instrução',
			'bannerHome1' => 'Banner Home 1',
			'bannerHome2' => 'Banner Home 2',
			'bannerHome3' => 'Banner Home 3'
		);
	}

}
