<?php

class TrabalheConoscoForm extends CFormModel
{
	public $nome;
	public $dataNascimento;
	public $sexo;
	public $estadoCivil;
	public $endereco;
	public $complemento;
	public $bairro;
	public $cidade;
	public $uf;
	public $cep;
	public $telefone;
	public $celular;
	public $email;
	public $cargo;
	public $formAcadGrauInst;
	public $formAcadCurso;
	public $formAcadInstit;
	public $formAcadAnoConcl;
	public $histProfEmpresa;
	public $histProfDataAdmis;
	public $histProfDataDeslig;
	public $histProfFuncao;
	public $histProfDescAtiv;
	public $histProfEmpresa1;
	public $histProfDataAdmis1;
	public $histProfDataDeslig1;
	public $histProfFuncao1;
	public $histProfDescAtiv1;
	public $qualificacoes;
	
	public function rules() {
		return array(
			array(
				'nome', 
				'required', 
				'message' => 'Obrigatório.'
			),
			array(
				'nome, dataNascimento, sexo, estadoCivil, endereco, complemento, bairro, cidade, uf, cep, 
				 telefone, celular, email, cargo, formAcadGrauInst, formAcadCurso, formAcadInstit, 
				 formAcadAnoConcl, qualificacoes', 
				'required', 
				'message' => 'Obrigatório.'
			),
			array(
				 'histProfEmpresa, histProfDataAdmis, histProfDataDeslig, histProfFuncao, histProfDescAtiv, 
				  histProfEmpresa1, histProfDataAdmis1, histProfDataDeslig1, histProfFuncao1, histProfDescAtiv1',
				  'safe'			
			),
			array(
				'email', 
				'email', 
				'message' => 'E-mail inválido.'
			)
		);
	}

	public function attributeLabels() {
		return array(
			'dataNascimento'		=> 'Data de Nascimento',	
			'endereco' 				=> 'Endereço',
			'uf' 					=> 'Estado',
			'cep' 					=> 'CEP',
			'email' 				=> 'E-mail',
			'formAcadGrauInst' 		=> 'Grau de Instrução',
			'formAcadCurso' 		=> 'Curso',
			'formAcadInstit' 		=> 'Instituição',
			'formAcadAnoConcl' 		=> 'Ano de Conclusão',
			'histProfEmpresa'  		=> 'Empresa',
			'histProfEmpresa1' 		=> 'Empresa',
			'histProfDataAdmis'  	=> 'Data de Admissão',
			'histProfDataAdmis1' 	=> 'Data de Admissão',
			'histProfDataDeslig'  	=> 'Data de Desligamento',
			'histProfDataDeslig1' 	=> 'Data de Desligamento',
			'histProfFuncao'  		=> 'Função',
			'histProfFuncao1' 		=> 'Função',
			'histProfDescAtiv'  	=> 'Atividades Desenvolvidas',
			'histProfDescAtiv1' 	=> 'Atividades Desenvolvidas',
			'qualificacoes'			=> 'Qualificações'
		);
	}
	
	/**
	 * @return array possíveis sexos.
	 */
	public function getSexoList()
	{
		$sexos = array('Feminino', 'Masculino');
		
		foreach ($sexos as $sexo) {
			$sexo = trim($sexo);
			$return[$sexo] = $sexo;
		}
		
		ksort($return);
		return $return;
	}
	
	/**
	 * @return array possíveis estados civis.
	 */
	public function getEstadoCivilList()
	{
		$sexos = array('Casado(a)', 'Solteiro(a)', 'Divorciado(a)', 'Viuvo(a)', 'Amasiado(a)');
		
		foreach ($sexos as $sexo) {
			$sexo = trim($sexo);
			$return[$sexo] = $sexo;
		}
		
		ksort($return);
		return $return;
	}
	
	/**
	 * @return array possíveis estados.
	 */
	public function getUfList()
	{
		$ufs = array(
			'AC' => 'Acre', 'AL' => 'Alagoas', 'AM' => 'Amazonas', 'AP' => 'Amapá', 'BA' => 'Bahia', 
			'CE' => 'Ceará', 'DF' => 'Distrito Federal', 'ES' => 'Espírito Santo', 'GO' => 'Goiás', 
			'MA' => 'Maranhão', 'MG' => 'Minas Gerais', 'MS' => 'Mato Grosso do Sul',
			'MT' => 'Mato Grosso', 'PA' => 'Pará', 'PB' => 'Paraíba', 'PE' => 'Pernambuco', 
			'PI' => 'Piauí', 'PR' => 'Paraná', 'RJ' => 'Rio de Janeiro', 
			'RN' => 'Rio Grande do Norte', 'RO' => 'Rondônia', 'RR' => 'Roraima', 
			'RS' => 'Rio Grande do Sul', 'SC' => 'Santa Catarina', 'SE' => 'Sergipe', 
			'SP' => 'São Paulo', 'TO' => 'Tocantins'
		);
		
		ksort($ufs);
		return $ufs;
	}
	
	/**
	 * @return array possíveis cargos com base nos parâmetros da aplicação.
	 */
	public function getCargoList()
	{
		$param = explode(',', Yii::app()->params['cargos']);
		
		foreach ($param as $cargo) {
			$cargo = trim($cargo);
			$cargos[$cargo] = $cargo;
		}
		
		ksort($cargos);
		return $cargos;
	}
	
	/**
	 * @return array possíveis graus de instrução de formações acadêmicas.
	 */
	public function getFormAcadGrauInstList()
	{
		$param = explode(',', Yii::app()->params['grausInstrucao']);
		
		foreach ($param as $cargo) {
			$cargo = trim($cargo);
			$cargos[$cargo] = $cargo;
		}
		
		ksort($cargos);
		return $cargos;
	}
}