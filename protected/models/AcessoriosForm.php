<?php

class AcessoriosForm extends CFormModel
{
	public $nome;
	public $tel;
	public $email;
	public $veiculo;
	public $acessorio;

	public function rules() {
		return array(
			array('nome', 'required', 'message'=>'O Nome é obrigatório.'),
			array('tel', 'required', 'message'=>'O Telefone é obrigatório.'),
			array('email', 'required', 'message'=>'O E-mail é obrigatório.'),
			array('veiculo', 'required', 'message'=>'O Veículo é obrigatório.'),
			array('acessorio', 'required', 'message'=>'O Acessório é obrigatório.'),				
			array('email', 'email', 'message'=>'O E-mail informado é inválido.'),
		);
	}

	public function attributeLabels() {
		return array(
			'nome'=>'Nome',
			'tel'=>'Telefone',
			'email'=>'E-mail',
			'veiculo'=>'Veículo',
			'acessorio'=>'Acessório',
		);
	}
}