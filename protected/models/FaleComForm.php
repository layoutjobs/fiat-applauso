<?php

class FaleComForm extends CFormModel
{
	public $nome;
	public $tel;
	public $email;
	public $mensagem;
	public $funcionario;
	public $produto;

	public function rules() {
		return array(
			array('nome', 'required', 'message' => 'O Nome é obrigatório.'),
			array('tel', 'required', 'message' => 'O Tel/celular é obrigatório.'),
			array('email', 'required', 'message' => 'O E-mail é obrigatório.'),	
			array('email', 'email', 'message' => 'O E-mail informado é inválido.'),
			array('mensagem', 'safe')
		);
	}

	public function attributeLabels() {
		return array(
			'nome' => 'Nome',
			'tel' => 'Tel/celular',
			'email' => 'E-mail',
			'mensagem' => 'Mensagem'
		);
	}
}