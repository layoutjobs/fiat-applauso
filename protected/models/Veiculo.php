<?php

/**
 * This is the model class for table "{{veiculo}}".
 */
class Veiculo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Veiculo the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{veiculo}}';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'nome, marca, anoModelo, anoLancto',
				'required'
			),
			array(
				'anoModelo, anoLancto, kmRodado, qtdePortas',
				'numerical',
				'integerOnly' => true
			),
			array(
				'prontaEntrega, ativo, promocao, novo, filtro',
				'boolean'
			),
			array(
				'nome, marca, modelo, cor, combustivel',
				'length',
				'max' => 45
			),
			array(
				'placa',
				'length',
				'max' => 8
			),
			array(
				'chassi',
				'length',
				'max' => 17
			),
			array(
				'anoModelo, anoLancto',
				'length',
				'max' => 4
			),
			array(
				'qtdePortas',
				'length',
				'max' => 1
			),
			array(
				'kmRodado',
				'length',
				'max' => 11
			),
			array(
				'valor',
				'length',
				'max' => 13
			),
			array(
				'imgDestaque',
				'length',
				'max' => 128
			),
			array(
				'acessorios',
				'length',
				'max' => 256
			),
			array(
				'pastaImgs',
				'unique'
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'id, placa, chassi, nome, marca, modelo, anoModelo, anoLancto',
				'safe',
				'on' => 'search'
			)
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'marca' => 'Marca',
			'modelo' => 'Modelo',
			'anoModelo' => 'Ano do modelo',
			'anoLancto' => 'Ano de fabricação',
			'kmRodado' => 'Km rodados',
			'qtdePortas' => 'Portas',
			'cor' => 'Cor',
			'acessorios' => 'Acessórios',
			'placa' => 'Placa',
			'chassi' => 'Chassi',
			'valor' => 'Valor (R$)',
			'novo' => 'Novo?',
			'prontaEntrega' => 'Pronta entrega?',
			'combustivel' => 'Combustível',
			'imgDestaque' => 'Imagem de destaque',
			'ativo' => 'Ativo?',
			'promocao' => 'Promoção?',
			'filtro' => 'Filtro?'
		);
	}
	
	/**
	 * @param string $localizacao a Localização, por exemplo, 'BRL' irá retornar R$ 9.999,99.
	 * @param string $padrao valor padrão caso não o valor esteja vazio.
	 * @return string valor formatado de acordo com a localização.
	 */
	public function getValor($localizacao = null, $padrao = null)
	{
		if (!$this->valor) 
			return $padrao ? $padrao : '';
		
		$valor = $this->valor;
		$valor = str_replace('.', '', $valor);
		$valor = str_replace(',', '.', $valor);	
		
		switch ($localizacao) {
			case 'BRL':
				return 'R$ ' . Yii::app()->numberFormatter->format('#,##0.00', $valor);
				break;
			default:
				return Yii::app()->numberFormatter->format('#,##0.00', $valor);
				break;
		}
	}
	
	/**
	 * @return string path (caminho) da pasta de upload.
	 */
	public function getPathUpload()
	{
		return 
			Yii::app()->getBasePath() . '/../' . 
			Yii::app()->params['upload']['pasta'] . '/' . 
			Yii::app()->params['veiculo']['pastaUpload'] . '/';
	}
	
	/**
	 * @return string url da pasta de upload.
	 */
	public function getUrlUpload()
	{
		return 
			// Yii::app()->request->hostInfo .
			Yii::app()->getBaseUrl() . '/' . 
			Yii::app()->params['upload']['pasta'] . '/' . 
			Yii::app()->params['veiculo']['pastaUpload'] . '/';
	}
	
	/**
	 * @param string $version. 
	 * @return array todas as imagens na pasta do veículo.
	 */
	public function getImgs($version = null)
	{
		$path = $this->getPathUpload() . $this->pastaImgs . '/';
		$imgs = array();

		if (is_dir($path)) {
			if ($handle = opendir($path)) {
				while (false !== ($filename = readdir($handle))) {
					if ($filename != '.' && $filename != '..' && !is_dir($path . $filename)) {
						$imgs[] = $filename;
					}
				}
				closedir($handle);
			}
		}
		
		if ($version && is_dir($path . $version)) {
			if ($handle = opendir($path . $version)) {
				while (false !== ($filename = readdir($handle))) {
					if ($filename != '.' && $filename != '..' && !is_dir($path . $version . '/' . $filename)) {
						$key = array_search($filename, $imgs);
						if ($key !== null)
							$imgs[$key] = $version . '/' . $filename;					
						else
							$imgs[] = $version . '/' . $filename;
					}
				}
				closedir($handle);
			}
		}
		
		return $imgs;
	}
	
	/**
	 * @param string $version. 
	 * @return array urls de todas as imagens na pasta do veículo.
	 */
	public function getUrlsImgs($version = null)
	{
		$imgs = array();
		foreach ($this->getImgs($version ? $version : null) as $img) {
			$imgs[] = $this->getUrlUpload() . $this->pastaImgs . '/' . $img;
		}
		return $imgs;
	}
	
	/**
	 * @param string $version. 
	 * @return string url da imagem em destaque.
	 */
	public function getUrlImgDestaque($version = null)
	{
		$path = $this->getPathUpload() . $this->pastaImgs . '/'; 
		if ($this->imgDestaque !== null && $version && is_file($path . $version . '/' . $this->imgDestaque))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $version . '/' . $this->imgDestaque;
		else if ($this->imgDestaque !== null && is_file($path . $this->imgDestaque))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $this->imgDestaque;	
		else
			return Yii::app()->getBaseUrl() . '/images/no-img.png';
	}
	
	/**
	 * @return string km rodados incluindo 'KM'.
	 */
	public function getKmRodado()
	{
		return $this->kmRodado != null ? $this->kmRodado . ' KM' : '';
	}
	
	/**
	 * @return string quantidade de portas incluíndo 'Portas'.
	 */
	public function getQtdePortas()
	{
		return $this->qtdePortas > 0 ? $this->qtdePortas . ' Portas' : '';
	}
	
	/**
	 * @param string a marca que deseja filtrar. Padrão é null, significa 
	 * que serão listadas todas as ocorrência do nome, independente da marca.
	 * @return array possíveis nomes de veículos com base em cadastros existentes.
	 */
	public function getNomeList($marca = null)
	{
		if ($marca === null) {
			// $nomes = Veiculo::model()->findAll('ativo = true AND filtro = false');
			$nomes = Veiculo::model()->findAll('ativo = true');
		} else {
			// $nomes = Veiculo::model()->findAll('marca = :marca AND ativo = true AND filtro = false',
			$nomes = Veiculo::model()->findAll('marca = :marca AND ativo = true',
				array(':marca' => $marca) 
			);
		}
	
		$nomes = CHtml::listData($nomes, 'nome', 'nome');
		ksort($nomes);
		
		return $nomes;
	}
	
	/**
	 * @return array possíveis marcas com base nos parâmetros da aplicação.
	 */
	public function getMarcaList()
	{
		$param = explode(',', Yii::app()->params['marcasVeiculo']);
		
		foreach ($param as $marca) {
			$marca = trim($marca);
			$marcas[$marca] = $marca;
		}
		ksort($marcas);
		
		return $marcas;
	}
	
	/**
	 * @return array possíveis anos de lançamento e modelo com base nos parâmetros da aplicação.
	 */
	public function getAnosList()
	{		
		$paramAnoDe  = Yii::app()->params['anoDeVeiculo'];
		$paramAnoAte = Yii::app()->params['anoAteVeiculo'];
		$anosRange   = range($paramAnoDe, $paramAnoAte);

		rsort($anosRange);
		
		foreach ($anosRange as $ano) {
			$anos[$ano] = $ano;
		}
		
		return $anos;
	}
	
	/**
	 * @return array possíveis quantidades de portas com base nos parâmetros da aplicação.
	 */
	public function getQtdePortasList()
	{
		$param = explode(',', Yii::app()->params['qtdesPortasVeiculo']);

		sort($param);
		
		foreach ($param as $qtdePortas) {
			$qtdePortas = trim($qtdePortas);
			$qtdesPortas[$qtdePortas] = $qtdePortas;
		}
		
		return $qtdesPortas;
	}
	
	/**
	 * @return array possíveis combustíveis com base nos parâmetros da aplicação.
	 */
	public function getCombustivelList()
	{
		$param = explode(',', Yii::app()->params['combustiveisVeiculo']);

		foreach ($param as $combustivel) {
			$combustivel = trim($combustivel);
			$combustiveis[$combustivel] = $combustivel;
		}
		ksort($combustiveis);
		
		return $combustiveis;
	}
	
	/**
	 * @param integer $length quantidade de caracteres a ser mostrado.
	 * O padrão é null, significa que retornará 0 caracters.
	 * @return string o resumo do conteúdo baseado na quantidade de caracteres.
	 */
	 public function getAcessorios($length = null)
	 {
	 	if ($length === null)
			return $this->acessorios;
		else
			if($this->acessorios != null) {
				$return  = trim(substr($this->acessorios, 0, $length));
				$return .= strlen($this->acessorios) - $length > 0 ? '...' : '';
				return $return;
			}
	 }
	
	/**
	 * This method is invoked after a model instance is created by new operator.
	 */
	protected function afterConstruct()
	{
		parent::afterConstruct();
		if ($this->isNewRecord)
			$this->marca = 'Fiat';
	}
	
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->filtro)
				$this->prontaEntrega = true;
				
			if ($this->prontaEntrega) {
				$this->kmRodado = 0;
				$this->novo = true;
			} else {
				$this->novo = false;	
			}		
							
			if ($this->valor > 0) {
				$this->valor = str_replace('.', '', $this->valor);
				$this->valor = str_replace(',', '.', $this->valor);	
			} else {
				$this->valor = null;
			}
			
			if (!is_file($this->getPathUpload() . $this->pastaImgs . '/' . $this->imgDestaque))
				$this->imgDestaque = '';
			else {
				$this->imgDestaque = str_replace('thumbnails/', '', $this->imgDestaque);
				$this->imgDestaque = str_replace('large/', '', $this->imgDestaque);
			}
			
			$this->nome = trim($this->nome);
			$this->modelo = trim($this->modelo);
			
			if ($this->isNewRecord)
				$this->pastaImgs = uniqid();
			
			return true;
		} else
			return false;
	}
	
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	protected function afterFind()
	{
		parent::afterFind();
		
		if ($this->valor !== null)
			$this->valor = Yii::app()->numberFormatter->format('#,##0.00', $this->valor);
	}
	
	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		parent::beforeDelete();
		
		foreach ($this->getImgs('thumbnails') as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		foreach ($this->getImgs('large') as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		foreach ($this->getImgs() as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		$path = $this->getPathUpload() . $this->pastaImgs;
		
		if (is_dir($path . '/thumbnails')) rmdir($path . '/thumbnails');
		if (is_dir($path . '/large')) rmdir($path . '/large');
		if (is_dir($path)) rmdir($path);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based
	 * on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		
		$criteria->compare('id', $this->id);
		$criteria->compare('placa', $this->placa, true);
		$criteria->compare('chassi', $this->chassi, true);
		$criteria->compare('nome', $this->nome, true);
		$criteria->compare('marca', $this->marca);
		$criteria->compare('modelo', $this->modelo, true);
		$criteria->compare('anoModelo', $this->anoModelo);
		$criteria->compare('anoLancto', $this->anoLancto);
		
		return new CActiveDataProvider($this, array(
			'pagination' => array(
				'pageSize' => '20'
			),
			'criteria' => $criteria
		));
	}
	
}
