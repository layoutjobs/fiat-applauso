<?php

/**
 * This is the model class for table "{{produto}}".
 */
class Produto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Produto the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{produto}}';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'nome',
				'required'
			),
			array(
				'ativo',
				'numerical',
				'integerOnly' => true
			),
			array(
				'nome, pastaImgs, imgDestaque',
				'length',
				'max' => 128
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'id, nome',
				'safe',
				'on' => 'search'
			)
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'ativo' => 'Ativo?',
			'pastaImgs' => 'Pasta de imagens',
			'imgDestaque' => 'Imagem de destaque'
		);
	}
	
	/**
	 * @return string path (caminho) da pasta de upload.
	 */
	public function getPathUpload()
	{
		return 
			Yii::app()->getBasePath() . '/../' . 
			Yii::app()->params['upload']['pasta'] . '/' . 
			Yii::app()->params['produto']['pastaUpload'] . '/';
	}

	/**
	 * @return string url da pasta de upload.
	 */
	public function getUrlUpload()
	{
		return 
			// Yii::app()->request->hostInfo .
			Yii::app()->getBaseUrl() . '/' . 
			Yii::app()->params['upload']['pasta'] . '/' . 
			Yii::app()->params['produto']['pastaUpload'] . '/';
	}

	/**
	 * @param string $version. 
	 * @return array todas as imagens na pasta do veículo.
	 */
	public function getImgs($version = null)
	{
		$path = $this->getPathUpload() . $this->pastaImgs . '/';
		$imgs = array();

		if (is_dir($path)) {
			if ($handle = opendir($path)) {
				while (false !== ($filename = readdir($handle))) {
					if ($filename != '.' && $filename != '..' && !is_dir($path . $filename)) {
						$imgs[] = $filename;
					}
				}
				closedir($handle);
			}
		}
		
		if ($version && is_dir($path . $version)) {
			if ($handle = opendir($path . $version)) {
				while (false !== ($filename = readdir($handle))) {
					if ($filename != '.' && $filename != '..' && !is_dir($path . $version . '/' . $filename)) {
						$key = array_search($filename, $imgs);
						if ($key !== null)
							$imgs[$key] = $version . '/' . $filename;					
						else
							$imgs[] = $version . '/' . $filename;
					}
				}
				closedir($handle);
			}
		}
		
		return $imgs;
	}

	/**
	 * @param string $version. 
	 * @return array urls de todas as imagens na pasta do veículo.
	 */
	public function getUrlsImgs($version = null)
	{
		$imgs = array();
		foreach ($this->getImgs($version ? $version : null) as $img) {
			$imgs[] = $this->getUrlUpload() . $this->pastaImgs . '/' . $img;
		}
		return $imgs;
	}
	
	/**
	 * @param string $version. 
	 * @return string url da imagem em destaque.
	 */
	public function getUrlImgDestaque($version = null)
	{
		$path = $this->getPathUpload() . $this->pastaImgs . '/'; 
		if ($this->imgDestaque !== null && $version && is_file($path . $version . '/' . $this->imgDestaque))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $version . '/' . $this->imgDestaque;
		else if ($this->imgDestaque !== null && is_file($path . $this->imgDestaque))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $this->imgDestaque;	
		else
			return Yii::app()->getBaseUrl() . '/images/no-img.png';
	}
		
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if (!is_file($this->getPathUpload() . $this->pastaImgs . '/' . $this->imgDestaque))
				$this->imgDestaque = '';
			else {
				$this->imgDestaque = str_replace('thumbnails/', '', $this->imgDestaque);
				$this->imgDestaque = str_replace('large/', '', $this->imgDestaque);
			}			
			
			if ($this->isNewRecord)
				$this->pastaImgs = uniqid();
			return true;
		} else
			return false;
	}
	
	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		parent::beforeDelete();
		
		foreach ($this->getImgs('thumbnails') as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		foreach ($this->getImgs('large') as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		foreach ($this->getImgs() as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		$path = $this->getPathUpload() . $this->pastaImgs;
		
		if (is_dir($path . '/thumbnails')) rmdir($path . '/thumbnails');
		if (is_dir($path . '/large')) rmdir($path . '/large');
		if (is_dir($path)) rmdir($path);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based
	 * on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria = new CDbCriteria;
		
		$criteria->compare('id', $this->id);
		$criteria->compare('nome', $this->nome);
		$criteria->order = 'nome';
		
		return new CActiveDataProvider($this, array(
			'pagination' => array(
				'pageSize' => '20'
			),
			'criteria' => $criteria
		));
	}
	
}
