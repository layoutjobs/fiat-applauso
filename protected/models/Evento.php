<?php

/**
 * This is the model class for table "{{evento}}".
 */
class Evento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Evento the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{evento}}';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'titulo, data, tipo',
				'required'
			),
			array(
				'tipo, ativo',
				'numerical',
				'integerOnly' => true
			),
			array(
				'pastaImgs, titulo, subtitulo, imgDestaque',
				'length',
				'max' => 128
			),
			array(
				'data',
				'date',
				'format' => 'dd/MM/yyyy'
			),
			array(
				'conteudo',
				'safe',
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'id, titulo, subtitulo, tipo',
				'safe',
				'on' => 'search'
			)
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo' => 'Título',
			'subtitulo' => 'Subtítulo',
			'conteudo' => 'Conteúdo',
			'data' => 'Data',
			'tipo' => 'Tipo',
			'ativo' => 'Ativo?',
			'pastaImgs' => 'Pasta de imagens',
			'imgDestaque' => 'Imagem de destaque'
		);
	}

	/**
	 * @return string path (caminho) da pasta de upload.
	 */
	public function getPathUpload()
	{
		return 
			Yii::app()->getBasePath() . '/../' . 
			Yii::app()->params['upload']['pasta'] . '/' . 
			Yii::app()->params['evento']['pastaUpload'] . '/';
	}
	
	/**
	 * @return string url da pasta de upload.
	 */
	public function getUrlUpload()
	{
		return 
			// Yii::app()->request->hostInfo .
			Yii::app()->getBaseUrl() . '/' . 
			Yii::app()->params['upload']['pasta'] . '/' . 
			Yii::app()->params['evento']['pastaUpload'] . '/';
	}
	
	/**
	 * @param string $version. 
	 * @return array todas as imagens na pasta do veículo.
	 */
	public function getImgs($version = null)
	{
		$path = $this->getPathUpload() . $this->pastaImgs . '/';
		$imgs = array();

		if (is_dir($path)) {
			if ($handle = opendir($path)) {
				while (false !== ($filename = readdir($handle))) {
					if ($filename != '.' && $filename != '..' && !is_dir($path . $filename)) {
						$imgs[] = $filename;
					}
				}
				closedir($handle);
			}
		}
		
		if ($version && is_dir($path . $version)) {
			if ($handle = opendir($path . $version)) {
				while (false !== ($filename = readdir($handle))) {
					if ($filename != '.' && $filename != '..' && !is_dir($path . $version . '/' . $filename)) {
						$key = array_search($filename, $imgs);
						if ($key !== null)
							$imgs[$key] = $version . '/' . $filename;					
						else
							$imgs[] = $version . '/' . $filename;
					}
				}
				closedir($handle);
			}
		}
		
		return $imgs;
	}
	
	/**
	 * @param string $version. 
	 * @return array urls de todas as imagens na pasta do veículo.
	 */
	public function getUrlsImgs($version = null)
	{
		$imgs = array();
		foreach ($this->getImgs($version ? $version : null) as $img) {
			$imgs[] = $this->getUrlUpload() . $this->pastaImgs . '/' . $img;
		}
		return $imgs;
	}
	
	/**
	 * @param string $version. 
	 * @return string url da imagem em destaque.
	 */
	public function getUrlImgDestaque($version = null)
	{
		$path = $this->getPathUpload() . $this->pastaImgs . '/'; 
		if ($this->imgDestaque !== null && $version && is_file($path . $version . '/' . $this->imgDestaque))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $version . '/' . $this->imgDestaque;
		else if ($this->imgDestaque !== null && is_file($path . $this->imgDestaque))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $this->imgDestaque;	
		else
			return Yii::app()->getBaseUrl() . '/images/no-img.png';
	}
	
	/**
	 * @param string $nome. 
	 * @param string $version. 
	 * @return string url de uma imagem específica.
	 */
	public function getImgUrl($img, $version = null)
	{
		$path = $this->getPathUpload() . $this->pastaImgs . '/'; 
		if ($version && is_file($path . $version . '/' . $img))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $version . '/' . $img;
		else if ($img !== null && is_file($path . $img))
			return $this->getUrlUpload() . $this->pastaImgs . '/' . $img;	
		else
			return Yii::app()->getBaseUrl() . '/images/no-img.png';
	}
	
	/**
	 * @return array possíveis tipos com base nos parâmetros da aplicação.
	 */
	public function getTipoList()
	{
		$tipos = Yii::app()->params['evento']['tipos'];
		
		return $tipos;
	}
	
	/**
	 * @return string o nome do tipo de acordo os parâmetros da aplicação.
	 */
	public function getTipo()
	{
		$param    = Yii::app()->params['evento']['tipos'];
		$param[0] = 'Indefinido';
		$tipo     = $param[$this->tipo];
		return $tipo;
	}
	
	/**
	 * @param integer $qtdeCaracteres quantidade de caracteres a ser mostrado.
	 * O padrão é null, significa que retornará 0 caracters.
	 * @return string o resumo do conteúdo baseado na quantidade de caracteres.
	 */
	 public function getResumoConteudo($qtdeCaracteres = null)
	 {
	 	return trim(substr($this->conteudo, 0, strlen($this->conteudo) - $qtdeCaracteres !== null ? $qtdeCaracteres : 0));
	 }
	
	/**
	 * This method is invoked after a model instance is created by new operator.
	 */
	protected function afterConstruct()
	{
		parent::afterConstruct();
		if ($this->isNewRecord)
			$this->data = Yii::app()->dateFormatter->format('dd/MM/yyyy', date('yyyy-MM-dd'));
	}
	
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			$data       = strtotime($this->data);
			$this->data = Yii::app()->dateFormatter->format('yyyy-MM-dd', $this->data);
			
			if (!is_file($this->getPathUpload() . $this->pastaImgs . '/' . $this->imgDestaque))
				$this->imgDestaque = '';
			else {
				$this->imgDestaque = str_replace('thumbnails/', '', $this->imgDestaque);
				$this->imgDestaque = str_replace('large/', '', $this->imgDestaque);
			}			
			
			if ($this->isNewRecord)
				$this->pastaImgs = uniqid();
			return true;
		} else
			return false;
	}
	
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	protected function afterFind()
	{
		parent::afterFind();
		$this->data = Yii::app()->dateFormatter->format('dd/MM/yyyy', $this->data);
	}
	
	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		parent::afterDelete();
		
		foreach ($this->getImgs('thumbnails') as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		foreach ($this->getImgs('large') as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		foreach ($this->getImgs() as $img)
			unlink($this->getPathUpload() . $this->pastaImgs . '/' . $img);
		
		$path = $this->getPathUpload() . $this->pastaImgs;
		
		if (is_dir($path . '/thumbnails')) rmdir($path . '/thumbnails');
		if (is_dir($path . '/large')) rmdir($path . '/large');
		if (is_dir($path)) rmdir($path);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based
	 * on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria = new CDbCriteria;
		
		$criteria->compare('id', $this->id);
		$criteria->compare('tipo', $this->tipo);
		$criteria->compare('titulo', $this->titulo, true);
		$criteria->compare('subtitulo', $this->subtitulo, true);
		$criteria->order = 'data desc';
		
		return new CActiveDataProvider($this, array(
			'pagination' => array(
				'pageSize' => '20'
			),
			'criteria' => $criteria
		));
	}
	
}
