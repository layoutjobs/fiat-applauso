<?php

class TestDriveForm extends CFormModel
{
	public $nome;
	public $tel;
	public $email;
	public $veiculo;

	public function rules() {
		return array(
			array('nome', 'required', 'message'=>'O Nome é obrigatório.'),
			array('tel', 'required', 'message'=>'O Tel/celular é obrigatório.'),
			array('email', 'required', 'message'=>'O E-mail é obrigatório.'),		
			array('email', 'email', 'message'=>'O E-mail informado é inválido.'),
		);
	}

	public function attributeLabels() {
		return array(
			'nome'=>'Nome',
			'tel'=>'Tel/celular',
			'email'=>'E-mail',
			'veiculo'=>'Veículo',
		);
	}
}