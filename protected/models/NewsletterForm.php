<?php

class NewsletterForm extends CFormModel
{
	public $email;

	public function rules() {
		return array(
			array('email', 'required', 'message'=>'O E-mail é obrigatório.'),			
			array('email', 'email', 'message'=>'O E-mail informado é inválido.')
		);
	}

	public function attributeLabels() {
		return array(
			'email'=>'E-mail'
		);
	}
}