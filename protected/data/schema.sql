-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tempo de Geração: 
-- Versão do Servidor: 5.5.27-log
-- Versão do PHP: 5.4.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `fiatapplauso`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_evento`
--

CREATE TABLE IF NOT EXISTS `tbl_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `conteudo` longtext NOT NULL,
  `titulo` varchar(128) NOT NULL,
  `subtitulo` varchar(128) NOT NULL,
  `data` date NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `pastaImgs` varchar(128) NOT NULL,
  `imgDestaque` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pastaImgs` (`pastaImgs`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `tbl_evento`
--

INSERT INTO `tbl_evento` (`id`, `updated`, `conteudo`, `titulo`, `subtitulo`, `data`, `tipo`, `ativo`, `pastaImgs`, `imgDestaque`) VALUES
(4, '2012-08-27 17:47:09', 'Destinado a ocupar o topo de linha da marca Fiat, o Freemont chega ao Brasil como o primeiro modelo resultante da associação da Fiat com o grupo Chrysler. Seu nome, assim como o próprio modelo, evoca o sentido de liberdade, do prazer da vida ao ar livre e da possibilidade de enfrentar qualquer situação da vida cotidiana com muita sofisticação. \r\n\r\nTrata-se de um veículo projetado para atender às diferentes exigências dos consumidores que buscam um veículo espaçoso, cômodo, seguro, completo, gostoso de dirigir e versátil para o dia a dia ou para o tempo livre nos finais de semana. No Fiat Freemont você encontra todos os carros que você quer.\r\n\r\nO moderno design do Fiat Freemont, fortemente caracterizado, apresenta formas marcantes, com linhas musculosas e ao mesmo tempo sofisticadas. Neste visual forte as lanternas horizontais traseiras com lâmpadas de LED revelam a grande variedade de tecnologia presente no modelo.  \r\n\r\nO novo veículo, produzido em Toluca, no México, e recentemente lançado na Europa, combina flexibilidade e funcionalidade com um estilo diferente e original. Suas dimensões garantem excelente conforto e comodidade interna, além de grande versatilidade: o Fiat Freemont é o único a oferecer 7 lugares em sua categoria no Brasil.', 'Freemont', 'Chegou a SUV da Fiat', '2012-09-28', 2, 1, '503bb29df0bd5', '505a304723705.jpg'),
(5, '2012-08-27 20:05:01', 'Destinado a ocupar o topo de linha da marca Fiat, o Freemont chega ao Brasil como o primeiro modelo resultante da associação da Fiat com o grupo Chrysler. Seu nome, assim como o próprio modelo, evoca o sentido de liberdade, do prazer da vida ao ar livre e da possibilidade de enfrentar qualquer situação da vida cotidiana com muita sofisticação. \r\n\r\nTrata-se de um veículo projetado para atender às diferentes exigências dos consumidores que buscam um veículo espaçoso, cômodo, seguro, completo, gostoso de dirigir e versátil para o dia a dia ou para o tempo livre nos finais de semana. No Fiat Freemont você encontra todos os carros que você quer.\r\n\r\nO moderno design do Fiat Freemont, fortemente caracterizado, apresenta formas marcantes, com linhas musculosas e ao mesmo tempo sofisticadas. Neste visual forte as lanternas horizontais traseiras com lâmpadas de LED revelam a grande variedade de tecnologia presente no modelo.  \r\n\r\nO novo veículo, produzido em Toluca, no México, e recentemente lançado na Europa, combina flexibilidade e funcionalidade com um estilo diferente e original. Suas dimensões garantem excelente conforto e comodidade interna, além de grande versatilidade: o Fiat Freemont é o único a oferecer 7 lugares em sua categoria no Brasil.', 'Doblô Xingu', 'Doblô com versão Xingu', '2012-09-19', 2, 1, '503bd2ed18712', ''),
(6, '2012-08-28 17:12:41', 'A Fiat Automóveis acaba de receber o título “Melhor Fabricante de Picape Compacta”, com o Fiat Strada, no prêmio Mérito Reconhecido 2012, organizado pela revista Jornauto.\r\n\r\nEsse resultado é levantado por meio de uma pesquisa realizada a cada dois anos com os leitores da revista. Nela são eleitos os maiores fabricantes e as melhores marcas de peças, produtos e veículos.\r\n\r\nEsse prêmio confirma o sucesso da picape Strada, líder em seu segmento há doze anos. Uma das responsáveis pelo grande êxito da Fiat Strada é a sua ampla oferta de modelos, cabines e motorizações.', 'Fiat recebe prêmio “Melhor Fabricante de Picape Compacta”', '', '2012-08-28', 3, 1, '503cfc09206a3', ''),
(7, '2012-08-29 19:14:20', 'A equipe Fiat Applàuso reuniu-se no dia 06 de Junho de 2012 na churrascaria O Costelão para comemorar mais uma recertificação em Padrões de Atendimento Fiat.\r\n\r\nO Projeto Padrões de Atendimento Fiat visa à implantação da Gestão por Processos em toda a rede de concessionárias da marca, buscando, como resultado, maior qualidade no atendimento prestado ao cliente. Anualmente as concessionárias Fiat passam por uma auditoria realizada pelo Instituto de Qualidade Automotiva (IQA) que resulta na aprovação ou não da certificação.\r\n\r\nDesde que a concessionária obteve sua primeira certificação, tem sido aprovada todos os anos, conquistando seu terceiro certificado. Resultado que só é possível com uma equipe dedicada e comprometida.\r\n\r\nParalelamente à entrega do certificado houve também o sorteio dos brindes conquistados como resultado do primeiro lugar (mês de janeiro) no Qualitas Excelência, programa que premia as 25 melhores concessionárias Fiat do Brasil.\r\n\r\nEstiveram presentes para a entrega do certificado os representantes do Regional Campinas.', 'Fiat Applàuso festeja o sucesso da 2ª Recertificação', '', '2012-06-06', 3, 1, '503e6a0c77cb3', ''),
(9, '2012-08-29 19:17:02', 'A Fiat Automóveis acaba de receber o título “Melhor Fabricante de Picape Compacta”, com o Fiat Strada, no prêmio Mérito Reconhecido 2012, organizado pela revista Jornauto.\r\n\r\nEsse resultado é levantado por meio de uma pesquisa realizada a cada dois anos com os leitores da revista. Nela são eleitos os maiores fabricantes e as melhores marcas de peças, produtos e veículos.\r\n\r\nEsse prêmio confirma o sucesso da picape Strada, líder em seu segmento há doze anos. Uma das responsáveis pelo grande êxito da Fiat Strada é a sua ampla oferta de modelos, cabines e motorizações.', 'Fiat recebe prêmio “Melhor Fabricante de Picape Compacta”', '', '2012-09-03', 2, 1, '503e6aae6c893', ''),
(10, '2012-09-03 18:25:07', 'O Grupo Fiat-Chrysler reduziu em 10% as emissões de dióxido de carbono (CO2) nos veículos produzidos durante o ano passado. Este documento é a primeira em que as corporações automobilísticas apresentam seus resultados em matéria de proteção ambiental e de melhora de eficiência de forma conjunta.\r\n\r\nSérgio Marchionne, presidente executivo do Grupo, explicou que o processo de integração das marcas continua com a determinação conjunta de incremento de responsabilidade e da harmonia com o meio ambiente e as comunidades onde atuam.\r\nMarchionne afirmou que “a sustentabilidade é uma parte integral de como ambas as organizações fazem seus negócios. Este informe foi auditado por parte da filial italiana da empresa SGS”.\r\n\r\nEntre os dados destacados do texto está o fato da Fiat e da Chrysler reduzirem em 18,5% o consumo de água na fabricação de seus veículos durante o ano passado. As empresas investiram 270 milhões de euros na melhoria da saúde e da segurança dos seus funcionários para melhorar o bem estar dos trabalhadores.', 'Fiat e Chrysler reduziram em 10% as emissões de CO2', '', '2012-03-09', 4, 1, '5044f603c491b', '505a2fe8ed1b8.jpg'),
(11, '2012-09-03 20:13:48', 'Confira os resultados alcançados antes da implantação do Sistema de Gestão Ambiental até o ano de 2011:\r\n\r\nCONSUMO DE ENERGIA ELÉTRICA\r\nDe 1994 a 2011 – redução de 56% (kWh/veículo)\r\n\r\nCONSUMO DE ÁGUA\r\nDe 1994 a 2011 – redução de 68% (m³/veículo)\r\n\r\nRECIRCULAÇÃO DE ÁGUA\r\n1994: 60%\r\n2011: 99%\r\n\r\nGERAÇÃO DE RESÍDUOS\r\nDe 1994 a 2011 – redução de 44% (Kg/veículo)\r\n\r\nREAPROVEITAMENTO DE RESÍDUOS\r\n1994: 70% \r\n2011: 100%\r\n\r\nUma das diretrizes da política ambiental da Fiat é levar a cultura de preservação para seus parceiros de negócios. Nesse sentido, a montadora vem buscando o aumento da conscientização das empresas fornecedoras, com a exigência de comprovação de licenciamentos perante os órgãos fiscalizadores, e observância de suas práticas ambientais.\r\n\r\nA montadora também tem ações de sensibilização junto aos funcionários, visando a redução do desperdício em todas as áreas da empresa, desde os vetores energéticos (água, ar comprimido, energia elétrica, vapor e combustíveis) até os hábitos pessoais dos empregados.', 'Sustentabilidade', '', '2012-09-02', 4, 1, '50450f7ce4ed7', '505a30992e317.jpg'),
(12, '2012-09-03 20:50:16', 'Depois de lançar o Novo Uno em 2010 e a quinta geração do Palio em 2011, a Fiat decidiu fazer de uma só vez a renovação da parte que ainda faltava de seu portfólio de compactos, ao apresentar versões repaginadas do sedã Siena EL, da perua Palio Weekend e da picape Strada.\r\n\r\nOs três modelos têm importância estratégica para a manutenção da liderança da Fiat no mercado brasileiro, pois são opções complementares aos seus carros de maior volume, caso do Uno e Palio. Siena e Palio Weekend representam 8,4% das vendas da Fiat no País e ajudam a manter a marca no topo do segmento de automóveis, no qual tem participação de 21,8%. Já a Strada é diretamente responsável pelo primeiro lugar em emplacamentos de comerciais leves, com fatia de 23,6%. No topo desse ranking há 12 anos seguidos, a Strada detém 47,7% das vendas de picapes compactas, que representam 35% dos licenciamentos de veículos utilitários.\r\n\r\nCom a chegada das novas versões, executivos da Fiat estimam que as vendas do conjunto todo cresçam cerca de 10%, algo como 2 mil unidades/mês a mais. Siena e Palio Weekend, que hoje vendem perto de 3,5 mil/mês (cada modelo), passariam ao patamar de 4 mil a 4,5 mil/mês. Já a Strada saltaria de 11 mil para 12 mil/mês. “Foi um desafio renovar todos de uma vez, mas com isso esperamos ganhar mais mercado, pois dessa forma eliminamos o adiamento da compra pelo consumidor que, normalmente, sempre espera pela próxima geração de produtos quando um só elemento da família é renovado”, explica Edison Mazzucato, diretor de marketing de produto.\r\n\r\nA estratégia empregada pela Fiat para manter a liderança no País é ser líder nos segmentos de maior volume. Para continuar assim, a marca vem agregando mais valor a cada geração que renova, com remodelação constante de design (externo e interno) e inclusão de equipamentos sem mexer substancialmente no preço dos produtos. Com os três agora relançados a tendência é a mesma.\r\n\r\nSiena EL, Strada e Weekend tiveram o design alinhados à nova identidade visual chamada de “Family Feeling Fiat”, uma forma de tornar todos os carros facilmente reconhecíveis pelo consumidor. Parte marcante dessa identidade está no “bigodinho” cromado com o círculo vermelho no centro que abriga a marca Fiat, instalado na grade dianteira, que começou a ser usado no novo Palio e agora está presente também nos outros três modelos da mesma família – a exceção é a versão de entrada da Strada, a Working, que manteve o mesmo desenho anterior da grade dianteira.', 'Fiat relança Siena EL, Palio Weekend e Strada de uma só vez', 'Siena EL, Strada e Weekend ganham novo visual e mais sofisticação', '2012-09-03', 2, 0, '504518080ecff', ''),
(13, '2012-09-04 17:56:37', '', 'Dia da Mulher Applàuso e Archy', '', '2012-09-04', 1, 1, '504640d54a225', ''),
(15, '2012-10-18 20:32:02', '', 'teste', '', '2012-10-31', 1, 1, '508067428dc93', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_funcionario`
--

CREATE TABLE IF NOT EXISTS `tbl_funcionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `nome` varchar(45) NOT NULL,
  `equipe` tinyint(1) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `ramal` smallint(4) NOT NULL,
  `email` varchar(45) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `imgDestaque` varchar(128) NOT NULL,
  `pastaImgs` varchar(128) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `perfil` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `tbl_funcionario`
--

INSERT INTO `tbl_funcionario` (`id`, `updated`, `nome`, `equipe`, `telefone`, `ramal`, `email`, `celular`, `imgDestaque`, `pastaImgs`, `ativo`, `perfil`) VALUES
(2, '0000-00-00 00:00:00', 'Vanda', 2, '(15) 3205-9400', 0, 'vanda@fiatapplauso.com.br', '(15) 9790-6813', 'vanda.png', '5045f80a64028', 1, ''),
(3, '0000-00-00 00:00:00', 'Renato', 2, '(15) 3205-9400', 0, 'renato@fiatapplauso.com.br', '(15) 9790-6776', 'renato.png', '5045f8a2d7aa2', 1, ''),
(4, '0000-00-00 00:00:00', 'Pedro', 2, '(15) 3205-9400', 0, 'pedro@fiatapplauso.com.br', '(15) 9175-9305', 'pedro.png', '5045f9e9aecf7', 1, ''),
(5, '0000-00-00 00:00:00', 'Mário', 2, '(15) 3205-9400', 0, 'mario.orsi@fiatapplauso.com.br', '(15) 9790-6848', 'mario.png', '5045ff92e4a30', 1, ''),
(6, '0000-00-00 00:00:00', 'Márcio', 2, '(15) 3205-9400', 0, 'joao@layoutnet.com.br', '(15) 9790-6921', 'marcio.png', '504602158f7c7', 1, ''),
(7, '0000-00-00 00:00:00', 'Eurides', 2, '(15) 3205-9400', 0, 'eurides@fiatapplauso.com.br', '(15) 9790-6789', 'eurides.png', '50460283c93b8', 1, ''),
(8, '0000-00-00 00:00:00', 'Ariane', 2, '(15) 3205-9400', 0, 'ariane.rodrigues@fiatapplauso.com.br', '(15) 9790-6786', 'ariane.png', '50460396e3b43', 1, ''),
(9, '0000-00-00 00:00:00', 'Camila', 1, '(15) 3205-9400', 208, 'camila@fiatapplauso.com.br', '', 'camila.png', '50462898b5412', 1, ''),
(10, '0000-00-00 00:00:00', 'Magno', 1, '(15) 3205-9400', 207, 'magno@fiatapplauso.com.br', '', 'magno.png', '50462a248dd5c', 1, ''),
(11, '0000-00-00 00:00:00', 'Nicélia', 1, '(15) 3205-9400', 208, 'nilceia@fiatapplauso.com.br', '', 'nicelia.png', '50462a9ab729a', 1, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_produto`
--

CREATE TABLE IF NOT EXISTS `tbl_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nome` varchar(128) NOT NULL,
  `imgDestaque` varchar(128) NOT NULL,
  `pastaImgs` varchar(128) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `tbl_produto`
--

INSERT INTO `tbl_produto` (`id`, `updated`, `nome`, `imgDestaque`, `pastaImgs`, `ativo`) VALUES
(1, '2012-09-13 21:12:56', 'Boné', '505a347ba73bf.jpg', '50524c585ba16', 1),
(2, '2012-09-19 21:11:26', 'Polo Piquet Masculina', '505a3581b3f99.jpg', '505a34feb8f2c', 1),
(4, '2012-10-18 19:29:01', 'dsdsdsd', '', '5080587d4bf9c', 1),
(5, '2012-10-18 19:29:06', 'dsdsdsd', '', '50805882c4f3e', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_veiculo`
--

CREATE TABLE IF NOT EXISTS `tbl_veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ativo` tinyint(1) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `marca` varchar(45) NOT NULL,
  `anoModelo` int(4) NOT NULL,
  `anoLancto` int(4) NOT NULL,
  `modelo` varchar(45) NOT NULL,
  `kmRodado` int(11) DEFAULT NULL,
  `qtdePortas` int(1) NOT NULL,
  `cor` varchar(45) NOT NULL,
  `acessorios` varchar(256) NOT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `novo` tinyint(1) NOT NULL,
  `prontaEntrega` tinyint(1) NOT NULL,
  `pastaImgs` varchar(128) NOT NULL,
  `imgDestaque` varchar(128) NOT NULL,
  `combustivel` varchar(45) NOT NULL,
  `promocao` tinyint(1) NOT NULL,
  `filtro` tinyint(1) NOT NULL,
  `placa` varchar(8) NOT NULL,
  `chassi` varchar(17) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pastaImgs` (`pastaImgs`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Extraindo dados da tabela `tbl_veiculo`
--

INSERT INTO `tbl_veiculo` (`id`, `updated`, `ativo`, `nome`, `marca`, `anoModelo`, `anoLancto`, `modelo`, `kmRodado`, `qtdePortas`, `cor`, `acessorios`, `valor`, `novo`, `prontaEntrega`, `pastaImgs`, `imgDestaque`, `combustivel`, `promocao`, `filtro`, `placa`, `chassi`) VALUES
(14, '2012-08-29 20:56:22', 1, 'Mille', 'Fiat', 2013, 2012, 'Economy', 0, 0, '', '', NULL, 0, 1, '503e81f665b0b', '5058d15fa4c11.png', '', 0, 1, '', ''),
(28, '2012-08-30 19:39:46', 1, 'Palio Fire', 'Fiat', 2013, 2012, 'Economy ', 0, 0, '', '', NULL, 1, 1, '503fc18220422', '5058d19ec9898.png', '', 0, 1, '', ''),
(29, '2012-08-30 19:41:30', 1, 'Palio', 'Fiat', 2013, 2012, '', 0, 0, '', '', NULL, 1, 1, '503fc1ea288d7', '5058d1c67c503.png', '', 0, 1, '', ''),
(30, '2012-08-30 19:43:48', 1, 'Palio Weekend', 'Fiat', 2013, 2012, '', 0, 0, '', '', NULL, 1, 1, '503fc27473cb1', '5058d1e8e85b7.png', '', 0, 1, '', ''),
(31, '2012-08-30 19:47:35', 1, 'Siena', 'Fiat', 2013, 2012, '', 0, 0, '', '', NULL, 1, 1, '503fc357ac543', 'Siena 2013.png', '', 0, 1, '', ''),
(32, '2012-08-30 19:53:55', 1, 'Palio Adventure', 'Fiat', 2013, 2012, '', 0, 0, '', '', NULL, 1, 1, '503fc4d30d37f', 'Palio Adventure 2013.png', '', 0, 1, '', ''),
(33, '2012-08-30 19:55:49', 1, 'Grand Siena', 'Fiat', 2013, 2012, '', 0, 0, '', '', NULL, 1, 1, '503fc54501030', 'Grand Siena 2013.png', '', 0, 1, '', ''),
(35, '2012-09-18 20:03:56', 1, 'Mille', 'Fiat', 2013, 2012, 'Economy ', 0, 0, 'Vermelho Alpine', '', '23860.00', 1, 1, '5058d3ac3add4', '5058d5dc32f3b.png', 'Flex', 0, 0, '', ''),
(36, '2012-09-19 14:15:02', 1, 'Corsa Sedan', 'Chevrolet', 2005, 2005, 'Maxx', NULL, 4, 'Preto', 'Trava elétrica, alarme e direção hidráulica.', '40000.00', 0, 0, '5059d366310da', '5059d4ca0fa4f (1).JPG', 'Flex', 0, 0, 'teste', ''),
(37, '2012-09-19 14:36:29', 1, 'Civic', 'Honda', 2003, 2003, 'LXS', NULL, 4, 'Dourado', 'Completíssimo.', '28000.00', 0, 0, '5059d86d3a1c0', '5059d91f6d0fa.JPG', 'Gasolina', 1, 0, 'DHS8038', ''),
(38, '2012-09-19 14:47:28', 1, 'Fox ', 'Volkswagen', 2008, 2009, 'Route 1.0', NULL, 4, 'Preto', 'Completo, todos os acessórios.', '34000.00', 0, 0, '5059db007a227', '5059db4866fcb.JPG', 'Flex', 1, 0, '', ''),
(39, '2012-09-19 14:55:35', 1, 'Doblô', 'Fiat', 2008, 2008, 'ADV 1.8', NULL, 4, 'Vermelho', 'Ar condicionado, trava elétrica, direção hidráulica e air bag.', '56000.00', 0, 0, '5059dce7c439e', '5059dd0d1df28.JPG', 'Flex', 1, 0, '', ''),
(40, '2012-10-22 19:39:36', 1, 'Corsa Sedan', 'Fiat', 2013, 2012, '', 0, 0, '', '', NULL, 1, 1, '5085a0f8d7df6', '', '', 0, 1, '', ''),
(41, '2012-10-23 11:35:05', 0, 'teste', 'Fiat', 2012, 2012, '', NULL, 0, '', '', NULL, 0, 0, '508680e9a23ed', '', '', 0, 0, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
