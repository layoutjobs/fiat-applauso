<?php

Yii::import('bootstrap.widgets.BootMenu');

class Menu extends BootMenu
{
	protected function isItemActive($item, $route)
	{
		// Elimina as hífens da url para que o item 
		// seja marcado como 'active' adequadamente.
		if (isset($item['url']))
			$item['url'][0] = str_replace('-', '', $item['url'][0]);
		
		if (isset($item['url']) && is_array($item['url']) && !strcasecmp(trim($item['url'][0], '/'), $route))
		{
			if (count($item['url']) > 1)
				foreach (array_splice($item['url'], 1) as $name=>$value)
					if (!isset($_GET[$name]) || $_GET[$name] != $value)
						return false;

			return true;
		}

		return false;
	}

}
