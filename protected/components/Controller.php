<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'main'.
	 */
	public $layout='main';
	/**
	 * @var array context menu items..
	 */
	public $menu=array();
	/**
	 * @var array context toolbar items.
	 */
	public $toolbar=array();
	/**
	 * @var array banner items.
	 */
	public $banner=array();
	/**
	 * @var boolean se o #content deve ser exibido. O padrão é true.
	 */
	public $content=true;
	/**
	 * @var boolean se o #footer deve ser exibido. O padrão é true.
	 */
	public $footer=true;
}