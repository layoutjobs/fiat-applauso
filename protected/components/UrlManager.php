<?php

class UrlManager extends CUrlManager
{
	public function parseUrl($request) {
		$url = parent::parseUrl($request);
		
		// Remove os hífens da url, necessário já que as Actions
		// correspondentes possuem e não aceitam hífen.
		return str_replace('-', '', $url);
	}

}
