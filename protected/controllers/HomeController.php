<?php

class HomeController extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'home'.
	 */
	public $layout = 'home';
	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF
			)
		);
	}
	
	/*
	 * Action default caso não esteja explicita nenhuma outra na url.
	 */
	public function actionIndex()
	{
		Yii::app()->user->setReturnUrl(Yii::app()->baseUrl);
		
		$model = new Veiculo();
		$model->unsetAttributes();
		
		// carros em promoção
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = true');
		$criteria->addCondition('filtro = false');
		$criteria->addCondition('promocao = true');
		$criteria->addCondition('imgDestaque <> \'\'');
		$criteria->order = 'rand()';
		
		$dataProvider = new CActiveDataProvider('Veiculo', array(
			'pagination' => array(
				'pageSize' => '3'
			),
			'criteria' => $criteria
		));
		
		// carros novos
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = true');
		$criteria->addCondition('filtro = true');
		$criteria->addCondition('prontaEntrega = true');
		$criteria->order = 'rand()';
		
		$carrosNovosModel = Veiculo::model()->findAll($criteria);
				
		$this->render('index', array(
			'model' => $model,
			'dataProvider' => $dataProvider,
			'carrosNovosModel' => $carrosNovosModel
		));
	}

	/*
	 * Exibe a página 'Assine nossas newsletters'.
	 */
	public function actionAssineNossasNewsletters()
	{
		if (Yii::app()->request->urlReferrer !== Yii::app()->request->hostInfo . Yii::app()->request->requestUri)
			Yii::app()->user->setReturnUrl(Yii::app()->request->urlReferrer);
			
		if (isset($_POST['email'])) {
			$newsletterForm = new NewsletterForm;
			$newsletterForm->email = $_POST['email'];
			if ($newsletterForm->validate()) {
				$message          = new YiiMailMessage;
				$message->view    = 'newsletter';
				$message->subject = 'Website > Assinatura de Newsletter';
				$message->from    = Yii::app()->params['emailContato'];
				$message->AddReplyTo($_POST['email']);
				
				foreach (explode(',', Yii::app()->params['emailNewsletter']) as $email) {
					$message->addTo(trim($email));			
				}

				$message->setBody(array('email' => $_POST['email']), 'text/html');
				
				Yii::app()->mail->send($message);	
				Yii::app()->user->setFlash('sucesso', '<strong>Obrigado!</strong> Sua solicitação foi enviada com sucesso.');				
			} else {
				Yii::app()->user->setFlash('erro', '<strong>Ops...</strong> ' . $newsletterForm->getError('email'));	
			}
		} 
		//$this->redirect(Yii::app()->user->returnUrl);	
		$this->redirect(Yii::app()->user->returnUrl);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
		
}
