<?php

class SegurosController extends Controller
{
	/*
	 * Action default caso não esteja explicita nenhuma outra na url.
	 * Nesse caso, redireciona para a actionAgoge().
	 */
	public function actionIndex()
	{
		$this->redirect(array(
			'agoge'
		));
	}
	
	/**
	 * Exibe a página 'Agoge'.
	 */
	public function actionAgoge()
	{		
		$this->render('agoge');	
	}
}