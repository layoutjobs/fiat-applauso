<?php

class EmpresaController extends Controller
{
	/**
	 * @var CActiveRecord a instância do model Evento atualmente carregado.
	 * @see loadEvento().
	 */
	private $_evento;
	
	/**
	 * @var CActiveRecord a instância do model Funcionario atualmente carregado.
	 * @see loadFuncionario().
	 */
	private $_funcionario;
	
	/*
	 * Action default. Nesse caso, redireciona para a actionQuemSomos().
	 */
	public function actionIndex()
	{
		$this->redirect(array(
			'quem-somos'
		));
	}
	
	/**
	 * Exibe a página 'Quem Somos'.
	 */
	public function actionQuemSomos()
	{		
		$this->render('quemSomos');	
	}
	
	/**
	 * Exibe a página 'Equipe de Vendas'.
	 */
	public function actionEquipeVendas()
	{		
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = 1');
		$criteria->addCondition('equipe = 2');
		$criteria->order = 'nome';
		
		$dataProvider = new CActiveDataProvider('Funcionario', array(
			'pagination' => array(
				'pageSize' => '999'
			),
			'criteria' => $criteria
		));
		
		$this->render('equipeVendas', array(
			'dataProvider' => $dataProvider
		));
	}
	
	/**
	 * Exibe a página 'Fale com ...'.
	 * @param string $vendedor nome do vendedor.
	 */
	public function actionFaleCom($vendedor)
	{		
		$criteria = new CDbCriteria;
			
		if (!empty($vendedor))
			$model = Funcionario::model()->findByAttributes(array('nome' => $vendedor), 'ativo = true');		
		
		if (empty($model))
			throw new CHttpException(404, 'Desculpe, não foi encontrado nenhum vendedor com o nome informado.');	
		
		$faleComForm = new FaleComForm;
		
		if (isset($_POST['FaleComForm'])) {
			$faleComForm->attributes = $_POST['FaleComForm'];
			if ($faleComForm->validate()) {
				$faleComForm->funcionario = $model;
				$this->_enviarFaleComForm($faleComForm);	
			}
		}

		$this->render('faleCom', array(
			'model' => $model,
			'faleComForm' => $faleComForm
		));
	}
	
	
	/**
	 * Exibe a página 'Galeria de fotos'.
	 */
	public function actionGaleria()
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = 1');
		$criteria->addCondition('tipo = 1');
		$criteria->order = 'data desc';
		
		$dataProvider = new CActiveDataProvider('Evento', array(
			'pagination' => array(
				'pageSize' => '10'
			),
			'criteria' => $criteria
		));
		
		$this->render('galeria', array(
			'dataProvider' => $dataProvider
		));
	}
	
	/**
	 * Exibe a página 'Meio Ambiente'.
	 */
	public function actionMeioAmbiente()
	{				
		$model = new Evento();
		$model->unsetAttributes();
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = 1');
		$criteria->addCondition('tipo = 4');
		$criteria->order = 'data desc';
		
		if (isset($_GET['id'])) {
			$model = $this->loadEvento();
		} else {
			if (!$model = Evento::model()->find($criteria))
				throw new CHttpException(404, 'Desculpe, Não há nenhum notícia no momento.');
		}
		
		$dataProvider = new CActiveDataProvider('Evento', array(
			'pagination' => array(
				'pageSize' => '5'
			),
			'criteria' => $criteria
		));
		
		$this->render('meioAmbiente', array(
			'model' => $model,
			'dataProvider' => $dataProvider
		));
	}

	/**
	 * Exibe a página 'Lançamentos'.
	 */
	public function actionLancamentos()
	{				
		$model = new Evento();
		$model->unsetAttributes();
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = 1');
		$criteria->addCondition('tipo = 2');
		$criteria->order = 'data desc';
		
		if (isset($_GET['id'])) {
			$model = $this->loadEvento();
		} else {
			if (!$model = Evento::model()->find($criteria))
				throw new CHttpException(404, 'Desculpe, Não há nenhum lançamento no momento.');
		}
		
		$dataProvider = new CActiveDataProvider('Evento', array(
			'pagination' => array(
				'pageSize' => '10'
			),
			'criteria' => $criteria
		));
		
		$this->render('lancamentos', array(
			'model' => $model,
			'dataProvider' => $dataProvider
		));
	}
	
	/**
	 * Exibe a página 'noticias'.
	 */
	public function actionNoticias()
	{				
		$model = new Evento();
		$model->unsetAttributes();
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = 1');
		$criteria->addCondition('tipo = 3');
		$criteria->order = 'data desc';
		
		if (isset($_GET['id'])) {
			$model = $this->loadEvento();
		} else {
			if (!$model = Evento::model()->find($criteria))
				throw new CHttpException(404, 'Desculpe, Não há nenhum lançamento no momento.');
		}
		
		$dataProvider = new CActiveDataProvider('Evento', array(
			'pagination' => array(
				'pageSize' => '10'
			),
			'criteria' => $criteria
		));
		
		$this->render('noticias', array(
			'model' => $model,
			'dataProvider' => $dataProvider
		));
	}
	
	/**
	 * Exibe a página 'Contato'.
	 */
	public function actionContato()
	{		
		$this->render('contato');	
	}
	
	/**
	 * Exibe a página 'Junte-se a nós'.
	 */
	public function actionTrabalheConosco()
	{
		$trabalheConoscoForm = new TrabalheConoscoForm;
	
		if (isset($_POST['TrabalheConoscoForm'])) {
			$trabalheConoscoForm->attributes = $_POST['TrabalheConoscoForm'];
			if ($trabalheConoscoForm->validate())
				$this->enviarTrabalheConoscoForm($trabalheConoscoForm);
		}
		
		$this->render('trabalheConosco', array(
			'trabalheConoscoForm' => $trabalheConoscoForm		
		));	
	}
	
	/**
	 * Retorna o model Evento baseado na PK obtida através do GET.
	 * Caso o evento não seja encontrado, um erro 404 é disparado.
	 */
	public function loadEvento()
	{
		if ($this->_evento === null) {
			if (isset($_GET['id'])) {
				$condition     = 'ativo = true';
				$this->_evento = Evento::model()->findByPk($_GET['id'], $condition);
			}
			if ($this->_evento === null)
				throw new CHttpException(404, 'O evento requisitado não foi encontrado.');
		}
		return $this->_evento;
	}
	
	/**
	 * Envia o formulário TrabalheConoscoForm.
	 */
	public function enviarTrabalheConoscoForm($model)
	{
		$message          = new YiiMailMessage;
		$message->view    = 'trabalheConosco';
		$message->subject = 'Website > Trabalhe Conosco';
		$message->from    = Yii::app()->params['emailContato'];
		$message->AddReplyTo($model->email, $model->nome);
		
		foreach (explode(',', Yii::app()->params['emailTrabalheConosco']) as $email) {
			$message->addTo(trim($email));			
		}
		
		$message->setBody(array('model' => $model), 'text/html');
		Yii::app()->user->setFlash('sucesso', '<strong>Obrigado!</strong> Sua mensagem foi enviada com sucesso. Por favor, aguarde o nosso contato.');
		Yii::app()->mail->send($message);
		$this->refresh();
	}
	
	/**
	 * Envia o formulário FaleCom.
	 */
	private function _enviarFaleComForm($model)
	{
		$message          = new YiiMailMessage;
		$message->view    = 'faleCom';
		$message->subject = 'Website > Fale com ' . $model->nome;
		$message->from    =  $model->funcionario->email;
		$message->addTo($model->funcionario->email);		
		$message->AddReplyTo($model->email, $model->nome);
		$message->setBody(array('model' => $model), 'text/html');
		Yii::app()->user->setFlash('sucesso', '<strong>Obrigado!</strong> Sua mensagem foi enviada com sucesso. Por favor, aguarde o nosso contato.');
		Yii::app()->mail->send($message);
		$this->refresh();
	}

}