<?php

class CarrosController extends Controller
{
	/**
	 * @var CActiveRecord a instância do model Veiculo atualmente carregado.
	 * @see loadVeiculo().
	 */
	private $_veiculo;
	
	/**
	 * @var CActiveRecord a instância do model Evento atualmente carregado.
	 * @see loadEvento().
	 */
	private $_evento;
	
	/*
	 * Action default. Nesse caso, redireciona para a actionSemiNovos().
	 */
	public function actionIndex()
	{
		$this->redirect(array('semiNovos'));
	}
			
	/**
	 * Exibe a página de listagem de carros à pronta-entrega ou a página
	 * específica do carro solicitado por meio do método GET, se for o caso.
	 */
	public function actionProntaEntrega()
	{
		$model = new Veiculo();
		$model->unsetAttributes();
		
		if (isset($_GET['id'])) {
			$model  = $this->loadVeiculo();
			
			$testDriveForm = new TestDriveForm;
						
			if (isset($_POST['TestDriveForm'])) {
				$testDriveForm->attributes = $_POST['TestDriveForm'];
				$testDriveForm->veiculo = $model;
				if ($testDriveForm->validate())
					$this->enviarTestDriveForm($testDriveForm);
			}
		} else {
			$criteria = new CDbCriteria;
			$criteria->addCondition('ativo = true');
			// $criteria->addCondition('filtro = false');
			$criteria->addCondition('novo = true');
			$criteria->addCondition('prontaEntrega = true');
			
			if (isset($_GET['marca']))
				$model->marca = $_GET['marca'];

			if (isset($_GET['modelo']))
				$model->nome = $_GET['modelo'];

			$criteria->compare('marca', $model->marca);			
			$criteria->compare('nome', $model->nome);
			
			$criteria->order = 'promocao desc, valor';
			
			$dataProvider = new CActiveDataProvider('Veiculo', array(
				'pagination' => array(
					'pageSize' => '10'
				),
				'criteria' => $criteria
			));
		}
		
		$this->render('prontaEntrega', array(
			'model' => $model,
			'dataProvider' => isset($dataProvider) ? $dataProvider : false,
			'testDriveForm' => isset($testDriveForm) ? $testDriveForm : false
		));
	}
	
	/**
	 * Exibe a página de listagem de carros semi-novos ou a página
	 * específica do carro solicitado por meio do método GET, se for o caso.
	 */
	public function actionSemiNovos()
	{	
		$model = new Veiculo();
		$model->unsetAttributes();
		
		if (isset($_GET['id'])) {
			$model  = $this->loadVeiculo();
			
			$testDriveForm = new TestDriveForm;
						
			if (isset($_POST['TestDriveForm'])) {
				$testDriveForm->attributes = $_POST['TestDriveForm'];
				$testDriveForm->veiculo = $model;
				if ($testDriveForm->validate())
					$this->enviarTestDriveForm($testDriveForm);
			}
		} else {
			$criteria = new CDbCriteria;
			$criteria->addCondition('ativo = true');
			// $criteria->addCondition('filtro = false');
			$criteria->addCondition('novo = false');
			
			if (isset($_GET['marca']))
				$model->marca = $_GET['marca'];

			if (isset($_GET['modelo']))
				$model->nome = $_GET['modelo'];

			$criteria->compare('marca', $model->marca);			
			$criteria->compare('nome', $model->nome);
			
			$criteria->order = 'promocao desc, valor';
			
			$dataProvider = new CActiveDataProvider('Veiculo', array(
				'pagination' => array(
					'pageSize' => '10'
				),
				'criteria' => $criteria
			));
		}
		
		$this->render('semiNovos', array(
			'model' => $model,
			'dataProvider' => isset($dataProvider) ? $dataProvider : false,
			'testDriveForm' => isset($testDriveForm) ? $testDriveForm : false
		));
	}
	
	/**
	 * Exibe a página de listagem de carros em promoção ou a página
	 * específica do carro solicitado por meio do método GET, se for o caso.
	 */
	public function actionPromocoes()
	{
		$model = new Veiculo();
		$model->unsetAttributes();
		
		if (isset($_GET['id'])) {
			$model  = $this->loadVeiculo();
			
			$testDriveForm = new TestDriveForm;
						
			if (isset($_POST['TestDriveForm'])) {
				$testDriveForm->attributes = $_POST['TestDriveForm'];
				$testDriveForm->veiculo = $model;
				if ($testDriveForm->validate())
					$this->enviarTestDriveForm($testDriveForm);
			}
		} else {
			$criteria = new CDbCriteria;
			$criteria->addCondition('ativo = true');
			// $criteria->addCondition('filtro = false');
			$criteria->addCondition('promocao = true');
			
			if (isset($_GET['marca']))
				$model->marca = $_GET['marca'];

			if (isset($_GET['modelo']))
				$model->nome = $_GET['modelo'];

			$criteria->compare('marca', $model->marca);			
			$criteria->compare('nome', $model->nome);
			
			$criteria->order = 'promocao desc, valor';
			
			$dataProvider = new CActiveDataProvider('Veiculo', array(
				'pagination' => array(
					'pageSize' => '10'
				),
				'criteria' => $criteria
			));
		}
		
		$this->render('promocoes', array(
			'model' => $model,
			'dataProvider' => isset($dataProvider) ? $dataProvider : false,
			'testDriveForm' => isset($testDriveForm) ? $testDriveForm : false
		));
	}

	/**
	 * Retorna o model Veiculo baseado na PK obtida através do GET.
	 * Caso o veículo não seja encontrado, um erro 404 é disparado.
	 */
	public function loadVeiculo()
	{
		if ($this->_veiculo === null) {
			if (isset($_GET['id'])) {
				// $condition      = 'ativo = true AND filtro = false';
				$condition      = 'ativo = true';
				$this->_veiculo = Veiculo::model()->findByPk($_GET['id'], $condition);
			}
			if ($this->_veiculo === null)
				throw new CHttpException(404, 'O veículo requisitado não foi encontrado.');
		}
		return $this->_veiculo;
	}
	
	/**
	 * Retorna o model Evento baseado na PK obtida através do GET.
	 * Caso o evento não seja encontrado, um erro 404 é disparado.
	 */
	public function loadEvento()
	{
		if ($this->_evento === null) {
			if (isset($_GET['id'])) {
				$condition     = 'ativo = true';
				$this->_evento = Evento::model()->findByPk($_GET['id'], $condition);
			}
			if ($this->_evento === null)
				throw new CHttpException(404, 'O evento requisitado não foi encontrado.');
		}
		return $this->_evento;
	}
	
	/**
	 * Envia o formulário de teste drive.
	 */
	public function enviarTestDriveForm($model)
	{
		$message          = new YiiMailMessage;
		$message->view    = 'testDrive';
		$message->subject = 'Website > Solicitação de Teste Drive';
		$message->from    = Yii::app()->params['emailContato'];
		$message->AddReplyTo($model->email, $model->nome);
		
		foreach (explode(',', Yii::app()->params['emailTestDrive']) as $email) {
			$message->addTo(trim($email));			
		}
	
		$message->setBody(array('model' => $model), 'text/html');
		Yii::app()->user->setFlash('sucesso', '<strong>Obrigado!</strong> Sua solicitação foi enviada com sucesso. Por favor, aguarde o nosso contato.');
		Yii::app()->mail->send($message);
		$this->refresh();
	}
	
	/**
	 * Entrega uma lista de nomes de veículos filtrados pela marca e
	 * encapsulados pela tag HTML 'option' para ser usado com um select.
	 */
	public function actionGetNomeAjax()
	{
		$model = Veiculo::model()->getNomeList($_POST['marca']);
		
		echo CHtml::tag('option', array('value' => ''), 'Modelo', true);
		
		foreach ($model as $value) {
        	echo CHtml::tag('option', array('value' => $value), CHtml::encode($value), true);			
		}
    }
	
}