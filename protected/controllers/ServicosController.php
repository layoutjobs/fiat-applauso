<?php

class ServicosController extends Controller
{
	/*
	 * Action default. Nesse caso, redireciona para a actionAssistenciaTecnica().
	 */
	public function actionIndex()
	{
		$this->redirect(array('assistencia-tecnica'));
	}
	
	/**
	 * Exibe a página 'Consórcio'.
	 */
	public function actionConsorcio()
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = true');
		$criteria->addCondition('filtro = true');
		$criteria->addCondition('prontaEntrega = true');
		$criteria->order = 'rand()';
		
		$carrosNovosModel = Veiculo::model()->findAll($criteria);

		$this->render('consorcio', array(
			'carrosNovosModel' => $carrosNovosModel
		));
	}
	
	/**
	 * Exibe a página 'Frotista'.
	 */
	public function actionFrotista()
	{
		$this->render('frotista');
	}
	
	/**
	 * Exibe a página 'Assistência Técnica'.
	 */
	public function actionAssistenciaTecnica()
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = 1');
		$criteria->addCondition('equipe = 1');
		$criteria->order = 'nome';
		
		$dataProvider = new CActiveDataProvider('Funcionario', array(
			'pagination' => array(
				'pageSize' => '999'
			),
			'criteria' => $criteria
		));
		
		$agendeRevisaoForm = new AgendeRevisaoForm;
		
		if (isset($_POST['AgendeRevisaoForm'])) {
			$agendeRevisaoForm->attributes = $_POST['AgendeRevisaoForm'];
			if ($agendeRevisaoForm->validate())
				$this->enviarAgendeRevisaoForm($agendeRevisaoForm);
		}
		
		$this->render('assistenciaTecnica', array(
			'agendeRevisaoForm' => $agendeRevisaoForm,
			'dataProvider' => $dataProvider
		));
	}
	
	/**
	 * Exibe a página 'Peças Originais e Acessórios'.
	 */
	public function actionPecasOriginais()
	{
		$acessoriosForm = new AcessoriosForm;
		
		if (isset($_POST['AcessoriosForm'])) {
			$acessoriosForm->attributes = $_POST['AcessoriosForm'];
			if ($acessoriosForm->validate())
				$this->enviarAcessoriosForm($acessoriosForm);
		}
		
		$this->render('pecasOriginais', array(
			'acessoriosForm' => $acessoriosForm
		));
	}
	
	/**
	 * Exibe a página 'Fiat Fashion'.
	 */
	public function actionFashion()
	{
		$model = new Produto();
		$model->unsetAttributes();
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = true');
		$criteria->order = 'nome';
		
		if (!$model = Veiculo::model()->find($criteria))
			throw new CHttpException(404, 'Desculpe, Não há nenhum produto no momento.');
		
		$dataProvider = new CActiveDataProvider('Produto', array(
			'pagination' => array(
				'pageSize' => '20'
			),
			'criteria' => $criteria
		));
		
		$this->render('fashion', array(
			'model' => $model,
			'dataProvider' => $dataProvider
		));
	}
	
	/**
	 * Exibe a página 'Agende sua Revisão'.
	 */
	public function actionAgendeRevisao()
	{
		$agendeRevisaoForm = new AgendeRevisaoForm;
		
		if (isset($_POST['AgendeRevisaoForm'])) {
			$agendeRevisaoForm->attributes = $_POST['AgendeRevisaoForm'];
			if ($agendeRevisaoForm->validate())
				$this->enviarAgendeRevisaoForm($agendeRevisaoForm);
		}
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('ativo = 1');
		$criteria->addCondition('equipe = 1');
		$criteria->order = 'nome';
		
		$dataProvider = new CActiveDataProvider('Funcionario', array(
			'pagination' => array(
				'pageSize' => '999'
			),
			'criteria' => $criteria
		));
		
		$this->render('agendeRevisao', array(
			'agendeRevisaoForm' => $agendeRevisaoForm,
			'dataProvider' => $dataProvider
		));
	}
	
	/**
	 * Envia o formulário AgendeRevisaoForm.
	 */
	public function enviarAgendeRevisaoForm($model)
	{
		$message          = new YiiMailMessage;
		$message->view    = 'agendeRevisao';
		$message->subject = 'Website > Agendamento de Revisão';
		$message->from    = Yii::app()->params['emailContato'];
		$message->AddReplyTo($model->email, $model->nome);
		
		foreach (explode(',', Yii::app()->params['emailRevisao']) as $email) {
			$message->addTo(trim($email));			
		}		
		
		$message->setBody(array('model' => $model), 'text/html');
		Yii::app()->user->setFlash('sucesso', '<strong>Obrigado!</strong> Sua solicitação foi enviada com sucesso. Por favor, aguarde o nosso contato.');
		Yii::app()->mail->send($message);
		$this->refresh();
	}
	
	/**
	 * Envia o formulário AcessoriosForm.
	 */
	public function enviarAcessoriosForm($model)
	{
		$message          = new YiiMailMessage;
		$message->view    = 'acessorios';
		$message->subject = 'Website > Acessório';
		$message->from    = Yii::app()->params['emailContato'];
		$message->AddReplyTo($model->email, $model->nome);
		
		foreach (explode(',', Yii::app()->params['emailAcessorio']) as $email) {
			$message->addTo(trim($email));			
		}
		
		$message->setBody(array('model' => $model), 'text/html');
		Yii::app()->user->setFlash('sucesso', '<strong>Obrigado!</strong> Sua solicitação foi enviada com sucesso. Por favor, aguarde o nosso contato.');
		Yii::app()->mail->send($message);
		$this->refresh();
	}
	
	/**
	 * Exibe a página 'Fale com/ Produto Fiat Fashion'.
	 * @param integer $vendedor ID vendedor.
	 */
	public function actionFashionFaleCom($produto)
	{		
		$criteria = new CDbCriteria;
			
		if (!empty($produto))
			$model = Produto::model()->findByAttributes(array('id' => $produto), 'ativo = true');		
		
		if (empty($model))
			throw new CHttpException(404, 'Desculpe, não foi encontrado nenhum vendedor com o nome informado.');	
		
		$faleComForm = new FaleComForm;
		
		if (isset($_POST['FaleComForm'])) {
			$faleComForm->attributes = $_POST['FaleComForm'];
			if ($faleComForm->validate()) {
				$faleComForm->produto = $model;
				$this->_enviarFashionFaleCom($faleComForm);	
			}
		}

		$this->render('fashionFaleCom', array(
			'model' => $model,
			'faleComForm' => $faleComForm
		));
	}
	
	/**
	 * Envia o formulário FaleCom.
	 */
	private function _enviarFashionFaleCom($model)
	{
		$message          = new YiiMailMessage;
		$message->view    = 'fashionFaleCom';
		$message->subject = 'Website > Informação de Produto Fiat Fashion';
		$message->from    =  $model->email;
		
		$emailsFashion = explode(',', Yii::app()->params['emailFashion']);
		
		foreach ($emailsFashion as $emailFashion) {
			$message->addTo(trim($emailFashion));	
		}
				
		$message->AddReplyTo($model->email, $model->nome);
		$message->setBody(array('model' => $model), 'text/html');
		Yii::app()->user->setFlash('sucesso', '<strong>Obrigado!</strong> Sua mensagem foi enviada com sucesso. Por favor, aguarde o nosso contato.');
		Yii::app()->mail->send($message);
		$this->refresh();
	}
}