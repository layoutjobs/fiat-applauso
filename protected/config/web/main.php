<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..',
	'name' => 'Fiat Applàuso',
	
	'defaultController' => 'home',
	
	'language' => 'pt_br',
	
	// preloading components
	'preload' => array(
		'log',
		'bootstrap'
	),
	
	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.widgets.*',
		'ext.mail.YiiMailMessage'
	),
	
	// application components
	'components' => array(
		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true
		),
		'coreMessages' => array(
			// necessário para o sistema ler os arquivos de mensagem na pasta /protected/messages.
			'basePath' => null
		),
		'clientScript' => array(
			'packages' => array(
				'galleryView' => array(
					'baseUrl' => 'packages/galleryView',
					'js' => array(
						'js/jquery.easing.1.3.js',
						'js/jquery.galleryview-3.0-dev.js',
						'js/jquery.timers-1.2.js'
					),
					'css' => array(
						'css/jquery.galleryview-3.0-dev.css'
					),
					'depends' => array(
						'jquery'
					)
				),
				'prettyPhoto' => array(
					'baseUrl' => 'packages/prettyPhoto',
					'js' => array(
						'js/jquery.prettyPhoto.js'
					),
					'css' => array(
						'css/prettyPhoto.css'
					),
					'depends' => array(
						'jquery'
					)
				),
				'bxSlider' => array(
					'baseUrl' => 'packages/bxSlider',
					'js' => array(
						'jquery.bxSlider.min.js',
						'jquery.easing.1.3.js'
					),
					'css' => array(
						'bx_styles/bx_styles.css'
					),
					'depends' => array(
						'jquery'
					)
				),
				'googleMapsAPI' => array(
					'baseUrl' => 'http://maps.googleapis.com/maps/api',
					'js' => array(
						'js?sensor=false&language=' . Yii::app()->language . '&region=' . Yii::app()->language,
					)
				)
			)
		),

		'urlManager' => array(
			'class' => 'UrlManager',
			'urlFormat' => 'path',
			'showScriptName' => false, // necessário .htaccess
			'rules' => array(
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			)
		),

		// mysql connection
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=site2',
			'emulatePrepare' => true,
			'username' => 'layout',
			'password' => 'Yt7d2Ty5aAlJp4',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_'
		),
		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => 'home/error'
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning'
				)
				// uncomment the following to show log messages on web pages
				
				/*
				array(
				'class'=>'CWebLogRoute',
				),
				*/
			)
		),
		'bootstrap' => array(
			'class' => 'ext.bootstrap.components.Bootstrap' // assuming you extracted bootstrap under extensions
		),
		'mail' => array(
			'class' => 'ext.mail.YiiMail',
			/*
			'transportType' => 'smtp',
			'transportOptions' => array(
				'host' => '',
				'username' => '',
				'password' => '',
				'port' => '25',
				'encryption' => 'ssl'
			),
			*/
			'viewPath' => 'application.views.mails',
			'logging' => true,
			'dryRun' => false
		)
	),
	
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => require(dirname(__FILE__) . '/params.php')
);