<?php

$file    = dirname(__FILE__) . '/../../data/params.inc';
$content = file_get_contents($file);
$arr     = unserialize(base64_decode($content));
return CMap::mergeArray(
	$arr,
	array(
		'salt'=>'P@bl0',

		/*
		 * Parâmetros estáticos.
		 */
		 
		// usuários: username => password
		'usuarios' => array(
			'admin' => 'admin',
		),
		
		// upload 
		'upload' => array(
			'pasta' => 'uploads'
		),
				
		'mapaZoom' => '16',
		'mapaZoomClick' => '18',
		
		// model Veiculo
		'veiculo' => array(
			'pastaUpload' => 'veiculos',
		),
		
		// model Funcionario		
		'funcionario' => array (
			'pastaUpload' => 'funcionarios',
			// cuidado, não mudar as ordem chaves já listadas.
			'equipes' => array(
				1 => 'Assistência Técnica',
				2 => 'Vendas'
			)	
		),
		
		// model Evento
		'evento' => array(
			'pastaUpload' => 'eventos',
			// cuidado, não mudar as ordem chaves já listadas.
			'tipos' => array(
				1 => 'Galeria',
				2 => 'Lançamento',
				3 => 'Notícia',
				4 => 'Meio Ambiente'
			)
		),
		
		// model Produto
		'produto' => array(
			'pastaUpload' => 'produtos',
		)
	)
);