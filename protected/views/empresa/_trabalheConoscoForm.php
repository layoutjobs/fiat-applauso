<div class="form">
	<?php 
	$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
		'id' => 'trabalhe-conosco-form',
		'type' => 'horizontal'
	)); 
	?>
	<fieldset>
		<legend>Dados Pessoais</legend>
		<?php echo $form->textFieldRow($model, 'nome', array('class' => 'span4')); ?>		
		<?php echo $form->textFieldRow($model, 'dataNascimento', array('class' => 'span4')); ?>
		<?php echo $form->dropDownListRow($model, 'sexo', $model->getSexoList(), array('class' => 'span4', 'empty' => 'Selecione')); ?>
		<?php echo $form->dropDownListRow($model, 'estadoCivil', $model->getEstadoCivilList(), array('class' => 'span4', 'empty' => 'Selecione')); ?>
		<?php echo $form->textFieldRow($model, 'endereco', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'complemento', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'bairro', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'cidade', array('class' => 'span4')); ?>
		<?php echo $form->dropDownListRow($model, 'uf', $model->getUfList(), array('class' => 'span4', 'empty' => 'Selecione')); ?>
		<?php echo $form->textFieldRow($model, 'cep', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'telefone', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'celular', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'email', array('class' => 'span4')); ?>
	</fieldset>
	<fieldset>
		<legend>Cargo Pretendido</legend>
		<?php echo $form->dropDownListRow($model, 'cargo', $model->getCargoList(), array('class' => 'span4', 'empty' => 'Selecione')); ?>
	</fieldset>	
	<fieldset>
		<legend>Formação Acadêmica</legend>
		<?php echo $form->dropDownListRow($model, 'formAcadGrauInst', $model->getFormAcadGrauInstList(), array('class' => 'span4', 'empty' => 'Selecione')); ?>
		<?php echo $form->textFieldRow($model, 'formAcadCurso', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'formAcadInstit', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'formAcadAnoConcl', array('class' => 'span4')); ?>
	</fieldset>
	<fieldset>
		<legend>Histórico Profissional</legend>
		<?php echo $form->textFieldRow($model, 'histProfEmpresa', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'histProfDataAdmis', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'histProfDataDeslig', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'histProfFuncao', array('class' => 'span4')); ?>
		<?php echo $form->textAreaRow($model, 'histProfDescAtiv', array('class' => 'span4', 'rows' => '3')); ?>
		<?php echo $form->textFieldRow($model, 'histProfEmpresa1', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'histProfDataAdmis1', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'histProfDataDeslig1', array('class' => 'span4')); ?>
		<?php echo $form->textFieldRow($model, 'histProfFuncao1', array('class' => 'span4')); ?>
		<?php echo $form->textAreaRow($model, 'histProfDescAtiv1', array('class' => 'span4', 'rows' => '3')); ?>
	</fieldset>
	<fieldset>
		<legend>Qualificações</legend>
		<?php echo $form->textAreaRow($model, 'qualificacoes', array('class' => 'span4', 'rows' => '3')); ?>
	</fieldset>		
	<div class="form-actions">
		<?php echo CHtml::htmlButton('Enviar', array('type' => 'submit')); ?>
	</div>
	<?php $this->endWidget(); ?>	
</div>