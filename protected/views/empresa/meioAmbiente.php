<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Meio Ambiente';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/empresa-meio-ambiente.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<div class="row">
		<div class="span8 col1">		
			<section class="page-content">
				<?php if ($model->imgDestaque != null) : ?>
					<?php echo CHtml::image($model->getUrlImgDestaque()); ?>
				<?php endif; ?>
				<time><?php echo str_replace('/', '.', CHtml::encode($model->data)); ?></time>
				<h2 class="title">
					<?php echo CHtml::encode($model->titulo); ?>
					<?php if ($model->subtitulo != null) : ?>
					<small><?php echo CHtml::encode($model->subtitulo); ?>.</small>
					<?php endif; ?>	
				</h2>
				<p><?php echo (str_replace(chr(13), '<br />', $model->conteudo)); ?></p>
			</section>
		</div>	
		<div class="span4 col2">
			<h3 class="title">Outras notícias</h3>
			<?php 
			$this->widget('bootstrap.widgets.BootListView', array(
				'dataProvider' => $dataProvider,
				'itemView' => '_meioAmbienteListItem',
				'template' => '{items}{pager}'
			)); 
			?>
			<?php echo CHtml:: image(Yii::app()->baseUrl . '/images/selo-sustentabilidade.jpg'); ?>
		</div>
	</div>
</article><!-- entry -->