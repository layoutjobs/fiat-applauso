<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Fale com ' . $model->nome;
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/empresa-equipe-vendas.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span3"><?php echo Chtml::image($model->getUrlImgDestaque(), $model->nome); ?></div>
			<div class="span5">
				<h5>Olá, meu nome é <strong><?php echo CHtml::encode($model->nome); ?></strong>.<br />Como posso ajudar você?</h5>
				<p>Para falar comigo utilize o número de telefone abaixo ou<br />o formulário ao lado. Estou aguardando o seu contato.</p>
				
				<p><strong>Tel: <?php echo CHtml::encode($model->telefone) . ($model->ramal != 0 ? ' - Ramal ' . CHtml::encode($model->ramal) : ''); ?> /	
					<?php if ($model->celular != null) : ?>
					Cel: <?php echo CHtml::encode($model->celular); ?><br />
					<?php endif; ?>
				</strong></p>
			</div>
			<div class="span4">
				<?php $this->renderPartial('_faleComForm', array('model' => $faleComForm)); ?>				
			</div>
		</div>
	</section>
</article><!-- entry -->