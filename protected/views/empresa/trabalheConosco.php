<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Junte-se a nós';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/empresa-trabalhe-conosco.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<?php $this->renderPartial('_trabalheConoscoForm', array('model' => $trabalheConoscoForm)); ?>
	</section>
</article><!-- entry -->