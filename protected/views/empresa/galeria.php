<?php
$this->pageTitle = Yii::app()->name . ' - Empresa/ Galeria de fotos';
$this->banner    = array(
	array('image' => Yii::app()->baseUrl . '/images/banners/carros-galeria.jpg'),
);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<p>Aqui você vai encontrar algumas imagens dos eventos que<br />
		acontecem na Fiat Applàuso. São eventos especiais que temos muito<br />
		prazer em fazer parte desse sucesso. Clique nas imagens para ampliá-las.</p>
	</section>
</article><!-- entry -->

<?php $this->widget('bootstrap.widgets.BootListView', array(
	'id' => 'galeria-list',
	'dataProvider' => $dataProvider,
	'itemView' => '_galeriaListItem',
	'template' => '{items}{pager}',
	'pagerCssClass'=>'pagination pagination-centered'
)); ?>

<?php Yii::app()->clientScript->registerPackage('prettyPhoto');	?>
<?php Yii::app()->clientScript->registerPackage('bxSlider'); ?>
<?php Yii::app()->clientScript->registerScript('galeria-list', '
$(\'a[rel^="prettyPhoto"]\').prettyPhoto();
'); ?>