<div class="form">	
	<?php $form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
		'id' => 'fale-com-form',
	)); ?>	
	
	<?php echo $form->textFieldRow($model, 'nome', array('placeholder' => 'Nome')); ?>
	
	<?php echo $form->textFieldRow($model, 'tel', array('placeholder' => 'Tel/celular')); ?>
	
	<?php echo $form->textFieldRow($model, 'email', array('placeholder' => 'E-mail')); ?>
	
	<?php echo $form->textAreaRow($model, 'mensagem', array('rows' => '5')); ?>
	
	<div><?php echo CHtml::htmlButton('Enviar', array('type' => 'submit')); ?></div>
	
	<?php $this->endWidget(); ?>
</div>