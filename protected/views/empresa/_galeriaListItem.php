<div class="item">
	<h5 class="title"><?php echo str_replace('/', '.', $data->data) ; ?> - <?php echo $data->titulo; ?></h5>
	<div class="bx-slider" id="galeria-list-item-<?php echo $data->id; ?>">
		<ul>
			<?php foreach($data->getImgs() as $img) : ?>
				<li>
					<a rel="prettyPhoto[<?php echo $data->id; ?>]" href="<?php echo $data->getImgUrl($img, 'large'); ?>">
						<?php echo CHtml::image($data->getImgUrl($img)); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>

<?php Yii::app()->clientScript->registerScript('galeria-list-item-' . $data->id, "
$('#galeria-list-item-$data->id ul').bxSlider({
	displaySlideQty: 5,
	moveSlideQty: 1
});"
); ?>