<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Contato';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/empresa-contato.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<p class="text-upper"><strong>Dúvidas/ informações, fale conosco</strong></p>
		<address>
			<span class="phone"><?php echo CHtml::encode(Yii::app()->params['foneContato']); ?></span>
			<?php echo CHtml::encode(Yii::app()->params['endereco']); ?><br />
			<strong><?php echo CHtml::link(Yii::app()->params['emailContato'], 'mailto:' . Yii::app()->params['emailContato']) ?></strong>
		</address>
		<div class="mapa"><div id="mapa" style="width: 938px; height: 318px;"></div></div>
	</section>
</article><!-- entry -->

<?php Yii::app()->clientScript->registerPackage('googleMapsAPI');	?>

<?php Yii::app()->clientScript->registerScript('mapa', "
function initialize(){
	var myOptions = {
       	zoom: " . Yii::app()->params['mapaZoom'] . ",
		center: new google.maps.LatLng(" . Yii::app()->params['mapaLat'] . ", " . Yii::app()->params['mapaLng'] . "),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById('mapa'), myOptions);
	var marker = new google.maps.Marker({
		position: map.getCenter(),
		map: map,
		title: '" . CHtml::encode(Yii::app()->name) . "'
    });
	google.maps.event.addListener(map, 'center_changed', function(){
		window.setTimeout(function(){
			map.panTo(marker.getPosition());
		}, 3000);
    });
	google.maps.event.addListener(marker,'click',function(){
		map.setZoom(" . Yii::app()->params['mapaZoomClick'] . ");
		map.setCenter(marker.getPosition());
	});
}
google.maps.event.addDomListener(window, 'load', initialize);	
"); ?>
