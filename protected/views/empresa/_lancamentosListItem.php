<div class="item">
	<a href="<?php echo $this->createUrl('lancamentos', array('id' => $data->id)); ?>">
		<div class="row-fluid">
			<div class="span5">
				<?php if ($data->imgDestaque != null) : ?>
				<?php echo CHtml::image($data->getUrlImgDestaque('thumbnails')); ?>
				<?php endif; ?>
			</div>
			<div class="span7">		
				<h5 class="title">
					<?php echo CHtml::encode($data->titulo); ?>
					<?php if ($data->subtitulo != null) : ?>
					<small><?php echo CHtml::encode($data->subtitulo); ?>.</small>
					<?php endif; ?>	
				</h5>
			</div>
		</div>
	</a>
</div>