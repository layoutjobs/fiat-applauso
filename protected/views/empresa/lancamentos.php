<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Lançamentos';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/empresa-lancamentos.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<div class="row">
		<div class="span7 col1">
			<section class="page-content">
				<h2 class="title">
					<?php echo CHtml::encode($model->titulo); ?>
					<?php if ($model->subtitulo != null) : ?>
					<small><?php echo CHtml::encode($model->subtitulo); ?>.</small>
					<?php endif; ?>	
				</h2>
				
				<?php if ($model->imgDestaque != null) : ?>
					<?php echo CHtml::image($model->getUrlImgDestaque('large')); ?>
				<?php endif; ?>
				
				<p><?php echo (str_replace(chr(13), '<br />', CHtml::encode($model->conteudo))); ?></p>
			</section>
		</div>	
		<div class="span5 col2">
			<h3 class="title">Saiba mais detalhes sobre os lançamentos FIAT.</h3>
			<?php 
			$this->widget('bootstrap.widgets.BootListView', array(
				'dataProvider' => $dataProvider,
				'itemView' => '_lancamentosListItem',
				'template' => '{items}{pager}'
			)); 
			?>
		</div>
	</div>
</article><!-- entry -->