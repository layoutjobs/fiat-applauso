<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Equipe de Vendas';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/empresa-equipe-vendas.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<h5>Equipe de Vendas</h5>
		
		<p>A Fiat Applàuso oferece uma equipe de vendas treinada e capacitada para atendê-lo 
		com agilidade e competência, auxiliando na realização do melhor negócio. 
		Conheça abaixo nossa equipe:</p>
		<?php $this->widget('bootstrap.widgets.BootListView', array(
			'dataProvider' => $dataProvider,
			'itemView' => '_equipeVendasListItem',
			'template' => '{items}{pager}'
		)); ?>
	</section>
</article><!-- entry -->