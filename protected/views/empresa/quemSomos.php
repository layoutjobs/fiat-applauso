<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Quem Somos';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/empresa-quem-somos.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span6 col1">			
				<p>A Concessionária Fiat Applàuso possuí um know how de mais de 10 anos<br /> 
				de experiência, oferecendo toda a qualidade da marca Fiat  somados a 
				competência de profissionais capacitados da cidade de Tatuí.</p>

				<p>Tatuí merece Applàuso. O nome da concessionária foi uma singela 
				homenagem a tradição musical da cidade. Porém, Tatuí merece aplauso<br />
				não apenas por sua tradição musical, mas também por sua 
				história, por sua estrutura e acima de tudo, por sua gente.</p>
				
				<p>A Fiat Applàuso possuí mais de 3.600m² de área construída, projetada 
				para oferecer conforto e agilidade, seja na venda de veículos novos e semi-novos, 
				como na prestação de serviços automotivos, como reparos, acessórios 
				e manutenção.</p>
				
				<p>A Fiat Applàuso através da tecnologia, competência, agilidade e acima de<br />
				tudo respeito, se orgulha de fazer parte do desenvolvimento dessa<br />
				cidade tão especial.</p>
				
				<p> Faça uma visita para conhecer um pouco mais dessa dedicação que a 
				Applàuso, a cada dia, aprimora para melhor atender você. </p>
				
				<h5 class="text-upper no-margin-bottom">Nossa Missão:</h5>
				<p>Ser uma das melhores concessionárias da rede Fiat, realizando o sonho<br />
				dos nossos clientes e preservando nosso maior patrimônio <strong>VOCÊ</strong>.</p>
				
				<h5 class="text-upper no-margin-bottom">Nossos Valores:</h5>
				<p>Honestidade, competência e profundo respeito pelo cliente, este é o principal 
				lema da <strong>Applàuso Veículos</strong>.</p>
			</div>
			<div class="span6 col2">	
				<?php echo CHtml::image(Yii::app()->baseUrl . '/images/applauso-veiculos-tatui-interna.jpg', 'Área interna da Fiat Applàuso em Tatuí/SP.'); ?>
				<?php echo CHtml::image(Yii::app()->baseUrl . '/images/applauso-veiculos-tatui-fachada.jpg', 'Fachada da Fiat Applàuso em Tatuí/SP.'); ?>
			</div>
		</div>
		
		<div class="empresas">
			<h3 class="text-center">A <strong>Fiat Applàuso</strong> faz parte de um pool de empresas:</h3>			

			<div class="row-fluid">
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-applauso-veiculos.jpg', 'Concessionária Fiat Applàuso'); ?>
					<p>Concessionária Fiat</p>
				</div>
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-applauso-motos.jpg', 'Concessionária de Motos Applàuso'); ?>
					<p>Concessionária de Motos</p>
				</div>
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-agoge.jpg', 'Seguradora Agogê'); ?>
					<p>Seguradora</p>
				</div>
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-viaregio.jpg', 'Auto Posto Via Regio'); ?>
					<p>Auto Posto</p>
				</div>		
			</div>
			<div class="row-fluid">
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-viacafe.jpg', 'Conveniências Via Café'); ?>
					<p>Loja de Conveniências</p>
				</div>
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-olaria.jpg', 'Auto Posto Olaria'); ?>
					<p>Auto Posto</p>
				</div>
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-lila.jpg', 'Lila Empreendimentos Imobiliários'); ?>
					<p>Imobiliária</p>
				</div>
				<div class="span3">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-rdv.jpg', 'RDV Lub'); ?>
					<p>Troca de Óleo</p>
				</div>
			</div>
		</div><!-- empresas-grupo -->
	</section>
</article><!-- entry -->