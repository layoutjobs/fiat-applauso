<div class="item">
	<time><?php echo str_replace('/', '.', CHtml::encode($data->data)); ?></time>
	<a href="<?php echo $this->createUrl('noticias', array('id' => $data->id)); ?>">
		<h5 class="title">
			<?php echo CHtml::encode($data->titulo); ?>
			
			<?php if ($data->subtitulo != null) : ?>
			<small><?php echo CHtml::encode($data->subtitulo); ?>.</small>
			<?php endif; ?>
		</h5>
	</a>
</div>