<?php
	$this->pageTitle = Yii::app()->name . ' - Seguros/ Agogê';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/seguros-agoge.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span8 col1">
				<p>A Agogê seguros foi criada especialmente para 
				proteger você e seus bens. Trabalhamos em parceria com as melhores segurados 
				do país, e oferecermos as melhores condições e facilidades. Tudo isso para 
				garantir tranquilidade e segurança para sua família, para sua casa e para 
				seu veículo. Atendimento ágil 24 horas. Venha conhecer nossos planos 
				personalizados.</p>
				
				<p class="phone">(15) 3205.9400 - Ramal 210</p>
			</div>
			<div class="span4 col2">
				<?php echo CHtml::image(Yii::app()->baseUrl . '/images/agoge-seguros.jpg', 'Agogê Seguros'); ?>	
			</div>
		</div>		
		<div class="row">
			<div class="span4">
				<section class="tipo-seguro">
					<h2 class="title">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/icon-seguro-residencial.jpg'); ?>
						Residencial
					</h2>
					<p>Seguro residencial habitual ou de veraneio, 
					que protege sua casa ou apartamento e tudo o que tem dentro do imóvel contra 
					incêndio, queda de raio, explosão, além das mais diversas coberturas, tais 
					como, roubo, dano elétrico, vendaval, perda ou pagamento de aluguel, 
					vidros, danos a terceiros, etc... , em um sistema flexível e 
					personalizado de contrataçao que se adapta às suas necessidades. 
					Ideal para manter seu patrimônio protegido e sua família tranquila.</p>
				</section>
			</div>
			<div class="span4">
				<section class="tipo-seguro">
					<h2 class="title">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/icon-seguro-pessoal.jpg'); ?>
						Pessoal
					</h2>
					<p>É o seguro perfeito para você que quer 
					proporcionar à sua família toda a proteção que ela merece. Completo e flexível, 
					possui várias opções de planos com ampla variedade de coberturas, para você 
					escolher a melhor solução de acordo com as suas necessidades. Além da facilidade 
					na contratação e de pagamento sem burocracia, também oferece assistência 
					complementares para aumentar ainda mais sua segurança.</p>	
				</section>
			</div>
			<div class="span4">
				<section class="tipo-seguro">
					<h2 class="title">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/icon-seguro-veiculo.jpg'); ?>
						Veículo
					</h2>
					<p>Temos o seguro feito sob medida para o seu carro. 
					Diversas opções de cobertura e bônus na renovação. Em caso de colisão, você ainda 
					tem 20% de desconto em oficinas autorizadas.<br />
					A Agogê Seguros tem o melhor custo benefício do mercado.</p>
				</section>				 
			</div>
		</div>
	</section>
</article><!-- entry -->