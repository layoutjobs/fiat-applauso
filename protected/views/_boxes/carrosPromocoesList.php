<section class="box box-carros-promocoes-list">
	<h2 class="box-title"> 
		<?php echo CHtml::link('PROMOÇÕES DA SEMANA - Carros a pronta-entrega com preços especiais.', $this->createUrl('/carros/promocoes')); ?></h2>				
	<?php 
	$this->widget('zii.widgets.CListView', array(
		'dataProvider' => $dataProvider,
		'itemView' => '../_boxes/_carrosPromocoesListItem',
		'template' => '<div class="row-fluid">{items}</div>',
		'emptyText' => ''
	)); 
	?>
</section>