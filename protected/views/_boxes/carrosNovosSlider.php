<div class="box-carros-novos-slider box bx-slider">
	<h2 class="box-title">Veículos 0km a pronta-entrega na Applàuso - Escolha o seu modelo.</h2>
	<ul id="box-carros-novos-slider">
		<?php foreach ($model as $item) : ?>	
		<?php if ($item->imgDestaque) : ?>
				
		<li>
			<a href="<?php echo $this->createUrl('/carros/pronta-entrega', array('marca' => $item->marca, 'modelo' => $item->nome)); ?>">
				<?php echo CHtml::image($item->getUrlImgDestaque()); ?>
				<h5 class="title"><?php echo CHtml::encode($item->nome); ?></h5>
			</a>
		</li>			
	
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
</div>
		
<?php Yii::app()->clientScript->registerPackage('bxSlider'); ?>
<?php 
Yii::app()->clientScript->registerScript('box-carros-novos-slider', '
$(\'#box-carros-novos-slider\').bxSlider({
	displaySlideQty: 5,
	moveSlideQty: 1
});
'); 
?>