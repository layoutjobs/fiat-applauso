<?php

$parentView = isset($parentView) ? $parentView : 'semiNovos';

switch ($parentView) {
	case 'promocoes':
		$action = 'promocoes';
		break;
		
	case 'prontaEntrega':
		$action = 'pronta-entrega';
		break;
	
	default:
		$action = 'semi-novos';		
		break;
} 
?>

<section class="box box-carros-busca-form mod-b">
		
	<?php if ($action === 'promocoes') : ?>
	
	<h2 class="box-title">Pesquisa<small>Carros em promoção</small></h2>
	
	<?php elseif ($action === 'pronta-entrega') : ?>	

	<h2 class="box-title">Pesquisa<small>Novos Pronta-entrega</small></h2>
	
	<?php else : ?>		
		
	<h2 class="box-title">Pesquisa<small>Carros Semi-novos</small></h2>
	
	<?php endif; ?>
	
	<div class="form">
		<?php 
		$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
			'id' => 'carros-busca-form',
			'action' => array('carros/' . $action),
			'method' => 'get'
		)); 
		?>
				
		<?php 
		echo $form->dropDownList($model, 'marca', $model->getMarcaList(), array(
			'name' => 'marca',
			'id' => 'carros-busca-form-marca',
			'ajax' => array(
				'type' => 'POST',
				'url' => $this->createUrl('carros/getNomeAjax'),
				'update' => '#carros-busca-form-modelo'
			),
			'empty' => 'Marca',
		));	
		?>

		<?php 
		echo $form->dropDownList($model, 'nome', $model->getNomeList($model->marca), array(
			'name' => 'modelo',
			'id' => 'carros-busca-form-modelo',
			'empty' => 'Modelo'
		)); 
		?>

		<?php echo CHtml::htmlButton('OK', array('type' => 'submit', 'name' => false)); ?>		
		
		<?php $this->endWidget(); ?>
	</div>
</section>	


<?php Yii::app()->clientScript->registerScript('carros-busca-form', "
$('#carros-busca-form').submit(function(){
	$.fn.yiiGridView.update('veiculo-grid', {
		data: $(this).serialize()
	});
	return false;
});
"); ?>
