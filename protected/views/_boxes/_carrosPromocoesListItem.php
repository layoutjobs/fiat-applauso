<div class="span4">
	<a href="<?php echo $this->createUrl('/carros/promocoes', array('id' => $data->id)); ?>">
		<div class="item">
			<?php echo CHtml::image($data->getUrlImgDestaque('thumbnails')); ?>
			<span class="valor laranja-opacity-7"><?php echo CHtml::encode($data->getValor('BRL')); ?></span>
			<div class="nome"><?php echo CHtml::encode($data->nome . ' - ' . $data->anoModelo); ?></div>
		</div>
	</a>
</div>