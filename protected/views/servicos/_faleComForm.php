<div class="form">	
	<?php $form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
		'id' => 'fale-com-form',
	)); ?>	
	
	<?php echo $form->textFieldRow($model, 'nome', array('placeholder' => 'Nome', 'class' => 'span4')); ?>
	
	<?php echo $form->textFieldRow($model, 'tel', array('placeholder' => 'Tel/celular', 'class' => 'span4')); ?>
	
	<?php echo $form->textFieldRow($model, 'email', array('placeholder' => 'E-mail', 'class' => 'span4')); ?>
	
	<?php echo $form->textAreaRow($model, 'mensagem', array('rows' => '5', 'class' => 'span4')); ?>
	
	<div><?php echo CHtml::htmlButton('Enviar', array('type' => 'submit')); ?></div>
	
	<?php $this->endWidget(); ?>
</div>