<section class="box mod-c agende-revisao-form">
	<p class="box-pre-title">Revisão Fiat Applàuso</p>
	<h2 class="box-title">Agende sua revisão</h2>
	<div class="form">
		<?php $form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
			'id' => 'agende-revisao-form',
		)); ?>
		<?php echo $form->textFieldRow($model,'nome', array('class' => 'input-xlarge')); ?>
		<?php echo $form->textFieldRow($model,'tel', array('class' => 'input-xlarge')); ?>
		<?php echo $form->textFieldRow($model,'email', array('class' => 'input-xlarge')); ?>
		<?php echo $form->textFieldRow($model,'veiculo', array('class' => 'input-xlarge')); ?>
		<div><?php echo CHtml::htmlButton('Enviar Mensagem', array('type' => 'submit')); ?></div>
		<?php $this->endWidget(); ?>	
	</div>			
</section>