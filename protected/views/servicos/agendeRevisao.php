<?php
	$this->pageTitle = Yii::app()->name . ' - Serviços/ Agende sua revisão';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/servicos-agende-revisao.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span6 col1">
				<h5>Auto Centro</h5>

				<p><strong>Fale com nossos consultores para dúvidas/informações:</strong></p>	
				
				<?php 
				$this->widget('bootstrap.widgets.BootListView', array(
					'dataProvider' => $dataProvider,
					'itemView' => '_assistenciaTecnicaListItem',
					'template' => '{items}{pager}'
				)); 
				?>
			</div>
			<div class="span6 col2">
				<?php $this->renderPartial('_agendeRevisaoForm',array('model'=>$agendeRevisaoForm)); ?>			
			</div>
		</div>
	</section>
</article><!-- entry -->