<?php
	$this->pageTitle = Yii::app()->name . ' - Serviços/ Frotista';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/servicos-frotista.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span6">
				<h5>Vendas Diretas - Empresas</h5>
				
				<p>A Marca FIAT oferece à Empresários, condições diferenciadas para
				aquisição de veículos com o faturamento direto da Montadora.</p>
				
				<p>Basta apresentar o CNPJ e o Contrato Social, os descontos
				concedidos pela montadora podem chegar a valores expressivos
				além da facilidade de se produzir um veículo totalmente
				personalizado.</p>
				
				<p>Não se preocupe se sua Empresa está iniciando as atividades,
				estamos prontos para ajudá-lo no crescimento de sua Empresa com
				preços competitivos e taxas de financiamento excepcionais.</p>
				
				<p>Colocamos à disposição, uma equipe de mecânicos especialmente
				treinados para cuidar do seu FIAT.</p>
				
				<p>Caso desejar, agende ainda hoje uma visita nossa em sua Empresa
				através do telefone:</p>
				
				<p><strong><?php echo CHtml::encode(Yii::app()->params['foneContato']); ?> ou email: 
				<?php echo CHtml::link(Yii::app()->params['emailFrotista'], 'mailto:' . Yii::app()->params['emailFrotista']); ?></strong></p>
			</div>
			<div class="span6">
				<h5>Vendas Diretas - Táxi</h5>
				
				<p>Excelentes condições para Taxistas com descontos imbatíveis, as
				melhores taxas de juros e aprovação de crédito em até 100% do
				valor solicitado, além da facilidade de recompra do seu automóvel
				semi-novo.</p>
				
				<p>Traga ainda hoje o seu táxi para uma avaliação e renove o seu
				veículo comprando um FIAT zero Km.</p>
				
				<p>Conheça o Siena Tetra Fuel, o novo lançamento da FIAT que
				revolucionou o mercado competitivo entre os taxistas.</p>
			</div>		
		</div>		
	</section>
</article><!-- entry -->