<?php
$this->pageTitle = Yii::app()->name . ' - Serviços/ Produto Fiat Fashion';
$this->banner    = array(
	array('image' => Yii::app()->baseUrl . '/images/banners/servicos-fashion.jpg'),
); 
?>
	
<article class="entry">	
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<p>O Fiat Fashion tem sua segmentação de mix de produtos baseada em linhas com 
		identidades próprias, adventure e streetwear, inspiradas nas próprias linhas dos 
		carros ou lifestile, trazendo para a moda todo o arrojo do design automotivo.</p>
		
		<p>As roupas e acessórios das coleções Fiat Fashion passam por processos especiais de 
		criação e produção. Com a colaboração de um estilista exclusivo, as peças são 
		produzidas com técnicas de aplicação inovadoras, estampas exclusivas e tecidos 
		macios e confortáveis, tudo isso com um toque moderno e despojado.</p>
	</section>
	
	<br />

	<h5>Ofertas</h5>
	<?php $this->widget('bootstrap.widgets.BootListView', array(
		'dataProvider' => $dataProvider,
		'itemView' => '_fashionListItem',
		'template' => "{items}\n{pager}",
		'pagerCssClass'=>'pagination pagination-centered'
	)); ?>
</article><!-- entry -->