<?php
	$this->pageTitle = Yii::app()->name . ' - Serviços/ Peças Originais e Acessórios';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/servicos-pecas-originais.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span6">
				<div style="margin-right: 30px;">
					<h5>Peças</h5>
					
					<p><strong>A Applàuso oferece peças Genuínas Fiat com mais de 10.000 ítens à sua disposição 
					para entrega.</strong>Garantimos uma rápida entrega e um melhor atendimento. E você ainda 
					conta com equipe experiente e especializada.</p>
	
					<p>Estamos capacitados para atender as solicitações de peças de oficinas mecânicas, companhias 
					de seguros, lojas de auto-peças, outras concessionárias e frotistas de acordo 
					com suas necessidades.</p>
					
					<br />
					
					<div class="pull-right"><?php echo CHtml::image(Yii::app()->baseUrl . '/images/gino-passione-ferramenta.jpg'); ?></div>	
				</div>
			</div>
			<div class="span6">
				<div style="margin-right: 30px;">				
					<h5>Acessórios</h5>
									
					<p>Mantenha a garantia e a originalidade do seu Fiat adquirindo toda a linha de acessórios
					na Applàuso: vidros e travas elétricas, alarmes, direção hidráulica, ar condicionado, 
					saias laterais e spoilers, cd-players, auto-falantes, rodas de liga esportivas, 
					Kit proteção (jg de tapete, friso lateral e protetor de carter), capota marítima, 
					insulfilm, engate para carretas, calhas de chuva, estribos laterias, quebra - mato 
					e muito mais.</p>
					
					<?php $this->renderPartial('_acessoriosForm',array('model'=>$acessoriosForm)); ?>
				</div>		
			</div>
		</div>
	</section>
</article><!-- entry -->

<hr />