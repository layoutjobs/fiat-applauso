<?php
	$this->pageTitle = Yii::app()->name . ' - Empresa/ Fale com/ Fiat Fashion';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/servicos-fashion.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span3">
				<?php echo Chtml::image($model->getUrlImgDestaque(), $model->nome, array('class' => 'thumbnail')); ?>
			</div>
			<div class="span9">
				<h3><?php echo CHtml::encode($model->nome); ?></h3>
				<p style="margin-bottom: 25px;">Fale conosco e obtenha mais informações de como adquirir o seu.<br />
				Ligue para <strong><?php echo CHtml::encode(Yii::app()->params['foneContato']); ?></strong>
				ou utilize o formulário ao lado.</p>
				<?php $this->renderPartial('_faleComForm', array('model' => $faleComForm)); ?>		
			</div>
		</div>
	</section>
</article><!-- entry -->