<section class="box mod-c acessorios-form">
	<div class="form">
		<?php $form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
			'id' => 'acessorios-form',
		)); ?>
		<?php echo $form->textFieldRow($model, 'nome', array('class' => 'input-xlarge')); ?>
		<?php echo $form->textFieldRow($model, 'tel', array('class' => 'input-xlarge')); ?>
		<?php echo $form->textFieldRow($model, 'email', array('class' => 'input-xlarge')); ?>
		<?php echo $form->textFieldRow($model, 'veiculo', array('class' => 'input-xlarge')); ?>
		<?php echo $form->textFieldRow($model, 'acessorio', array('class' => 'input-xlarge')); ?>
		<div><?php echo CHtml::htmlButton('Enviar Mensagem', array('type' => 'submit')); ?></div>
		<?php $this->endWidget(); ?>	
	</div>			
</section>