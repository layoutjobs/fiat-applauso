<?php
	$this->pageTitle = Yii::app()->name . ' - Serviços/ Assistência Técnica';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/servicos-assistencia-tecnica.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<div class="row">
			<div class="span6 col1">
				<h5>Auto Centro</h5>
				
				<p>A Applàuso Veículos coloca a sua inteira disposição nosso Fiat Autocentro, onde você, 
				nosso cliente, encontra transparência nos orçamentos, bom atendimento, qualidade nos 
				serviços e a segurança que somente a montadora e sua rede autorizada podem oferecer. 
				Tudo isso com preços competitivos e facilidade de pagamento.</p>

				<p><strong>No Fiat Autocentro da Applàuso você tem:</strong></p>
							
				<ul>
					<li>Agilidade e pronto atendimento.</li>
					<li>Mão de obra especializada.</li>
					<li>Peças genuínas Fiat com um ano de garantia.</li>
					<li>Serviços com valores diferenciais em amortecedores, freios, filtros, troca de 
					óleo, baterias, alinhamento e  balanceamento e muito mais.</li>
				</ul>
				
				<p><strong>Serviços:</strong></p>
				
				<ul>
					<li>Pronto atendimento;</li>
					<li>Check-up gratuito;</li>
					<li>Kits de serviços com valores diferenciados;</li>
					<li>Mão de obra especializada e peças genuínas Fiat, com garantia 
					nacional de 1 ano;</li>
					<li>Preços especiais de escapamentos e amortecedores;</li>
					<li>Troca de óleos e filtros;</li>
					<li>Troca de bateria;</li>
					<li>Troca de pastilhas e discos de freio;</li>
					<li>Alinhamento e balanceamento de rodas.</li>
				</ul>
			</div>
			<div class="span6 col2">
				<div class="box mod-b" style="margin-top: 0;">
					<h5>Sempre Novo</h5>
									
					<p>Novo conceito FIAT de reparo rápido em funilaria e pintura, para deixar seu carro novo de 
					novo. Os trabalhos são realizados em boxes específicos, onde um profissional devidamente 
					treinado, efetua, com ferramentas e produtos especiais, pequenos reparos na lataria 
					e pintura dos veículos.</p>
					
					<p><strong>Principais reparos realizados:</strong></p>
					
					<ul>
						<li>Pequenos riscos;</li>
						<li>Manchas na pintura;</li>
						<li>"Chuva de granizo";</li>
						<li>Danos causados por pedras;</li>	
						<li>Pequenos amassados.</li>				
					</ul>
					
					<p><strong>Vantagens:</strong></p>
					
					<ul>
						<li>Originalidade da pintura do veículo;</li>
						<li>Melhoria do aspecto estético;</li>
						<li>Menor tempo com o veículo parado na concessionária;</li>
						<li>Menor custo de reparo;</li>
						<li>Valorização do veículo na hora da venda;</li>
						<li>Serviços rápidos de funilaria e pintura com um custo inferior 
						à franquia do seguro.</li>
					</ul>
				</div>				
			</div>
		</div>
		
		<hr />
		
		<div class="row">
			<div class="span6">
				<div class="box mod-c">
					<p class="box-pre-title">Fale com nossos consultores para dúvidas/informações:</p>
					<h2 class="box-title">Consultores</h2>
					<?php 
					$this->widget('bootstrap.widgets.BootListView', array(
						'dataProvider' => $dataProvider,
						'itemView' => '_assistenciaTecnicaListItem',
						'template' => '{items}{pager}'
					)); 
					?>
				</div>
			</div>
			<div class="span6">
				<?php $this->renderPartial('_agendeRevisaoForm', array('model' => $agendeRevisaoForm)); ?>			
			</div>
		</div>
	</section>
</article><!-- entry -->