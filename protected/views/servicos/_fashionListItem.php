<div class="item">
	<div class="img"><?php echo CHtml::image($data->getUrlImgDestaque()); ?></div>
	<h5 class="title"><?php echo CHtml::encode($data->nome); ?></h5>
	<?php $this->widget('bootstrap.widgets.BootButton', array(
	    'buttonType' => 'link',
	    'label' => 'Info',
	    'icon' => 'info-sign white',
	    'type' => 'warning',
	    'size' => 'mini',
	    'url' => array('servicos/fashion-fale-com', 'produto' => $data->id)
	)); ?>
</div>