<?php
	$this->pageTitle = Yii::app()->name . ' - Serviços/ Consórcio';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/servicos-consorcio.jpg'),
	);
?>

<article class="entry">
	<header class="page-header">
		<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
		<?php $this->renderPartial('/_boxes/compartilhar'); ?>
	</header>
	<section class="page-content">
		<h5>Consórcio Nacional Fiat - O Consórcio Fiat de Fábrica</h5>
		
		<p>O Consórcio Fiat é a reunião de pessoas físicas e/ou jurídicas em um grupo 
		fechado, em que os consorciados pagam parcelas mensais, com objetivo de formar 
		poupança, que será destinada à compra de um bem (veículo) novo ou usado.</p>
		
		<p>A autorização para atuar no sistema de consórcio é concedida pelo Banco 
		Central do Brasil (Bacen).</p>

		<h5>Benefícios do produto</h5>
		
		<ul>
			<li>No momento da adesão não se aplica taxa;</li>
			<li>O lance pode ser diluído, o que permite a redução da parcela mensal;</li>
			<li>Oferece o Seguro de Vida e Proteção Financeira, mais segurança para você e sua família;</li>
			<li>Sorteios realizados pela Loteria Federal;</li>
			<li>Antecipação de prestações, viabilizando a redução do valor das parcelas mensais;</li>
			<li>Data de vencimento fixo das suas parcelas;</li>
			<li>Garantia total de entrega do veículo pela Fiat Automóveis S.A.;</li>
			<li>Oferece os  Serviços On-line, proporcionando ao consorciado conveniência e agilidade na 
			consulta a informações e solicitação de serviços referentes ao grupo e cota.</li>
		</ul>
		
		<h5>Planos ofertados</h5>
		
		<p>O Consórcio Fiat é a forma ideal para você adquirir seu veículo Fiat, em  planos 
		para grupos novos e em andamento. Basta escolher o plano que mais lhe agrada e clicar 
		em "Consultar" outros prazos.</p>
		
		<h5>Mega Plano Consórcio Fiat</h5>
		
		<p>O Mega Plano Consórcio Fiat é um consórcio que dá mais chances de contemplação 
		mensais, que oferece toda a garantia para você e sua família com seguro desemprego 
		e de vida e com parcelas ajustadas à sua necessidade.</p>
		
		<p><strong>Escolha o modelo do seu carro novo e LIGUE: 
		<?php echo CHtml::encode(Yii::app()->params['foneContato']); ?>, ramal 209.</strong></p>
	</section>
</article><!-- entry -->

<?php $this->renderPartial('../_boxes/carrosNovosSlider', array('model' => $carrosNovosModel)); ?>