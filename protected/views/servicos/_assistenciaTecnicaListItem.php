<div class="item">
	<div class="row-fluid">
		<div class="span4"><?php echo Chtml::image($data->getUrlImgDestaque(), $data->nome); ?></div>
		<div class="span8">
			<h5><?php echo CHtml::encode($data->nome); ?></h5>
			<p>
				Tel: <?php echo CHtml::encode($data->telefone) . ($data->ramal != 0 ? ' - Ramal ' . CHtml::encode($data->ramal) : ''); ?><br />
				
				<?php if ($data->celular) : ?>
				Cel: <?php echo CHtml::encode($data->celular); ?><br />
				<?php endif; ?>
				
				<?php if ($data->email) : ?>
				
				<a class="btn btn-mini" href="<?php echo $this->createUrl('/empresa/falecom', array('vendedor' => $data->nome)); ?>">
					<i class="icon-chat"></i> Fale comigo
				</a>
				
				<?php endif; ?>
			</p>
		</div>
	</div>
</div>