<?php $this->renderPartial('/layouts/_header'); ?> 

<div id="page">
	<div id="main">
		<div id="content">				
			<?php if ($this->content) : ?>
				
			<?php echo $content; ?>
			
			<?php endif; ?>
		</div><!-- content -->
	</div>
</div><!-- page -->

<?php $this->renderPartial('/layouts/_footer'); ?> 