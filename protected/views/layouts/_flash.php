<?php if(Yii::app()->user->hasFlash('erro') || Yii::app()->user->hasFlash('sucesso')) : ?>

<section id="flash">
	<div class="container">
		<?php if(Yii::app()->user->hasFlash('sucesso')): ?>
		<div class="flash alert alert-success">
			<?php echo Yii::app()->user->getFlash('sucesso'); ?>
		</div>
		<?php endif; ?>
		
		<?php if(Yii::app()->user->hasFlash('erro')): ?>
		<div class="flash alert alert-error">
			<?php echo Yii::app()->user->getFlash('erro'); ?>
		</div>
		<?php endif; ?>
	</div>
</section><!-- flash -->

<?php endif; ?>