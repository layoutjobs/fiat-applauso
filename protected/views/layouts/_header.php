<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="pt-BR" />
	<meta name="author" content="Layout - Criação e Publicidade" />
	
	<meta name="title" content="<?php echo CHtml::encode(Yii::app()->params['metaTitle']); ?>" />
	<meta name="description" content="<?php echo CHtml::encode(Yii::app()->params['metaDescription']); ?>">	
	<meta name="keywords" content="<?php echo CHtml::encode(Yii::app()->params['metaKeywords']); ?>" />
	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/main.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/' . $this->id . '.css'); ?>
	
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" />
	
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-35757558-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
			
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="<?php echo strtolower($this->id) . ' ' . strtolower($this->action->id); ?>">
	<header id="header">
		<div class="container">
			<a class="logo pull-left" href="<?php echo Yii::app()->baseUrl; ?>" title="<?php echo CHtml::encode(Yii::app()->name); ?>">
				<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo.png', CHtml::encode(Yii::app()->name)); ?>
			</a>
			<div class="menu pull-left">
				<span>Applàuso Concessionária Fiat Tatuí - <?php echo CHtml::encode(Yii::app()->params['foneContato']); ?>
				<a class="facebook" href="<?php echo Yii::app()->params['linkFacebook']; ?>" title="Curta a Fiat Applàuso no Facebook" target="_blank">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/icon-facebook.png', 'Curta a Fiat Applàuso no Facebook'); ?>
				</a>
				</span>
				<nav id="mainmenu">
					<?php 
					$this->widget('Menu', 
						array(				
							'type' => 'pills',
							'items' => array(
								array('label' => 'Home', 'url' => array('/home')),
								array(
									'label' => 'Carros', 
									'items' => array(
										array('label' => 'Novos Pronta-entrega', 'url' => array('/carros/pronta-entrega')),
										array('label' => 'Semi-novos', 'url' => array('/carros/semi-novos')),
										array('label' => 'Promoções', 'url' => array('/carros/promocoes'))
									)
								),
								array(
									'label' => 'Serviços', 
									'items' => array(
										array('label' => 'Assistência Técnica', 'url' => array('/servicos/assistencia-tecnica')),
										array('label' => 'Agende sua revisão', 'url' => array('/servicos/agende-revisao')),
										array('label' => 'Peças Originais', 'url' => array('/servicos/pecas-originais')),
										array('label' => 'Frotista', 'url' => array('/servicos/frotista')),
										array('label' => 'Consórcio', 'url' => array('/servicos/consorcio')),
										array('label' => 'Fiat Fashion', 'url' => array('/servicos/fashion'))
									)
								),
								array(
									'label' => 'Quem Somos', 
									'items' => array(
										array('label' => 'Quem Somos', 'url' => array('/empresa/quem-somos')),
										array('label' => 'Equipe de Vendas', 'url' => array('/empresa/equipe-vendas')),
										array('label' => 'Meio Ambiente', 'url' => array('/empresa/meio-ambiente')),
										array('label' => 'Lançamentos', 'url' => array('/empresa/lancamentos')),
										array('label' => 'Noticias', 'url' => array('/empresa/noticias')),
										array('label' => 'Galeria de fotos', 'url' => array('/empresa/galeria')),
										array('label' => 'Contato', 'url' => array('/empresa/contato')),
										array('label' => 'Junte-se a nós', 'url' => array('/empresa/trabalhe-conosco'))
									)
								),
								array(
									'label' => 'Seguro', 
									'items' => array(
										array('label' => 'Agogê', 'url' => array('/seguros/agoge'))
									)
								)
							)
						)
					); 
					?>
				</nav><!-- mainmenu -->
			</div>
			<a class="logo-fiat pull-right" href="<?php echo Yii::app()->baseUrl; ?>" title="Applàuso Concessionária Fiat Tatuí">
				<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-fiat.png', 'Applàuso Concessionária Fiat Tatuí'); ?>
			</a>
		</div>
	</header><!-- header -->
	
	<?php if ($this->banner && $this->layout != 'home' && $this->layout != 'iframe') : ?>
	
	<section id="banner">
		<div class="container">
			<?php $this->widget('bootstrap.widgets.BootCarousel', array(
			    'items' => $this->banner,
			    'displayPrevAndNext' => (count($this->banner) > 1) ? true : false,
			    'htmlOptions' => array('class' => 'slide')
			)); ?>
		</div>
	</section>
	
	<?php endif; ?>
	
	<?php if ($this->layout != 'main') : ?>
	
	<?php $this->renderPartial('/layouts/_flash'); ?> 
	
	<?php endif ?>