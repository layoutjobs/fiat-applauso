<?php if ($this->footer) : ?>

<footer id="footer">
	<section class="newsletter">
		<div class="container">
			<div class="row">
				<div class="span5">
					<a href="<?php echo Yii::app()->createUrl('/empresa/equipe-vendas'); ?>">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/icon-deficiente-56.png', '', array('class' => 'pull-left')); ?>
						<h5>Isenção de impostos</h5>	
						<p>Consulte as condições especiais<br />
						para portadores de deficiência.</p>
					</a>
				</div>
				<div class="span4">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/icon-news-56.png', '', array('class' => 'pull-left')); ?>
					<h5>News Fiat Applàuso</h5>	
					<p>Cadastre seu e-mail para<br />
					receber nossa newsletter:</p>
				</div>
				<div class="span3">
					<?php echo CHtml::form($this->createUrl('home/assine-nossas-newsletters'), 'post'); ?>	
					<div class="input-append">
          				<?php echo CHtml::textField('email', '', array('placeholder' => 'digite o seu e-mail')); ?>
          				<?php echo CHtml::htmlButton('Enviar', array('type' => 'submit', 'class' => 'btn')); ?>
        			</div>
					<?php echo CHtml::endform() ?> 	
				</div>
			</div>
		</div>
	</section>	
	<section class="main">
		<div class="container">
			<div class="row">
				<div class="span2">
					<h5>Carros</h5>
					<?php $this->widget('Menu', array(
						'type' => 'pills',
						'stacked' => true,
						'items' => array(
							array('label' => 'Novos Pronta-entrega', 'url' => array('/carros/pronta-entrega')),
							array('label' => 'Semi-novos', 'url' => array('/carros/semi-novos')),
							array('label' => 'Promoções', 'url' => array('/carros/promocoes'))
						),
					)); ?>
				</div>
				<div class="span2">
					<h5>Serviços</h5>
					<?php $this->widget('Menu', array(
						'type' => 'pills',
						'stacked' => true,
						'items' => array(
							array('label' => 'Assistência Técnica', 'url' => array('/servicos/assistencia-tecnica')),
							array('label' => 'Agende sua revisão', 'url' => array('/servicos/agende-revisao')),
							array('label' => 'Peças Originais', 'url' => array('/servicos/pecas-originais')),
							array('label' => 'Frotista', 'url' => array('/servicos/frotista')),
							array('label' => 'Consórcio', 'url' => array('/servicos/consorcio')),
							array('label' => 'Fiat Fashion', 'url' => array('/servicos/fashion'))
						),
					)); ?>
				</div>
				<div class="span2">
					<h5>Empresa</h5>
					<?php $this->widget('Menu', array(
						'type' => 'pills',
						'stacked' => true,
						'items' => array(
							array('label' => 'Quem Somos', 'url' => array('/empresa/quem-somos')),
							array('label' => 'Equipe de Vendas', 'url' => array('/empresa/equipe-vendas')),
							array('label' => 'Meio Ambiente', 'url' => array('/empresa/meio-ambiente')),
							array('label' => 'Lançamentos', 'url' => array('/empresa/lancamentos')),
							array('label' => 'Noticias', 'url' => array('/empresa/noticias')),
							array('label' => 'Galeria de fotos', 'url' => array('/empresa/galeria')),
							array('label' => 'Contato', 'url' => array('/empresa/contato')),
							array('label' => 'Junte-se a nós', 'url' => array('/empresa/trabalhe-conosco'))
						),
					)); ?>
				</div>
				<div class="span2">
					<h5>Seguros</h5>
					<?php $this->widget('Menu', array(
						'type' => 'pills',
						'stacked' => true,
						'items' => array(
							array('label' => 'Agogê', 'url' => array('/seguros/agoge'))
						)
					)); ?>
				</div>
				<div class="span4">
					<div class="facebook pull-right">
					<div class="fb-like-box" data-href="<?php echo Yii::app()->params['linkFacebook']; ?>" data-width="260" data-height="218" data-show-faces="true" data-stream="false" data-header="true"></div>						
					</div>
				</div><!-- facebook -->
			</div>
			<div class=row>
				<div class="span3">
					<a class="logo" href="<?php echo Yii::app()->baseUrl; ?>" title="<?php echo CHtml::encode(Yii::app()->name); ?>">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo.png', CHtml::encode(Yii::app()->name), array('height' => '50')); ?>
					</a>
				</div>
				<div class="span4">
					<div class="phone"><?php echo CHtml::encode(Yii::app()->params['foneContato']); ?></div>			
				</div>
				<div class="span4">
					<address>
						<strong>e-mail: <?php echo CHtml::link(Yii::app()->params['emailContato'], 'mailto:' . Yii::app()->params['emailContato']); ?></strong><br />
						<?php echo CHtml::encode(Yii::app()->params['endereco']); ?>
					<address>
				</div>	
				<div class="span1">
					<a href="<?php echo Yii::app()->baseUrl; ?>" title="Applàuso Concessionária Fiat Tatuí">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/logo-fiat.png', 'Applàuso Concessionária Fiat Tatuí', array('height' => '50')); ?>
					</a>			
				</div>
			</div>
		</div>
	</section>
	<div id="copy">
		<div class="container">
			<p>Copyright &copy; <?php echo date('Y'); ?>. <?php echo CHtml::encode(Yii::app()->name); ?>. Todos os direitos reservados.</p>
		</div>
	</div>
</footer><!-- footer -->
<?php endif; ?>

<span class='st_email_hcount' displayText='Email'></span>
<span class='st_facebook_hcount' displayText='Facebook'></span>
<span class='st_twitter_hcount' displayText='Tweet'></span>
<span class='st_plusone_hcount' displayText='Google +1'></span>

<div id="ajax-loading"></div>
<?php
Yii::app()->clientScript->registerScript('ajax-loading', '
jQuery(\'#ajax-loading\').bind(\'ajaxSend\',function(){
	$(\'body\').addClass(\'loading\');
}).bind(\'ajaxComplete\',function(){
	$(\'body\').removeClass(\'loading\');
});
');
?>

<div id="fb-root"></div>
<?php 
Yii::app()->clientScript->registerScript('fb-like-box', '
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
	fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));
');
?>

</body>
</html>