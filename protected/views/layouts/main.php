<?php $this->renderPartial('/layouts/_header'); ?> 

<div id="page">
	<div id="main">
		<div class="container">
			<div id="content">							
				<?php $this->renderPartial('/layouts/_flash'); ?> 
				
				<?php if ($this->content) : ?>
					
				<?php echo $content; ?>
				
				<?php endif; ?>	
			</div><!-- content -->		
		</div>
	</div><!-- main -->
</div><!-- page -->

<section id="bottom">
	<div class="container">
		<div class="box banners">
			<div class="row">
				<div class="span">
					<a href="<?php echo Yii::app()->createUrl('/seguros/agoge'); ?>">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-agoge.png'); ?>
					</a>
				</div>
				<div class="span">
					<a href="<?php echo Yii::app()->createUrl('/servicos/assistencia-tecnica'); ?>">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-assistencia-tecnica.png'); ?>
					</a>
				</div>
				<div class="span">
					<a href="<?php echo Yii::app()->createUrl('/carros/pronta-entrega', array('marca' => 'fiat', 'modelo' => 'ducato')); ?>">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-ducato.png'); ?>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="span">
					<a href="<?php echo Yii::app()->createUrl('/servicos/fashion'); ?>">
						<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-fashion.png'); ?>
					</a>
				</div>
			</div>
		</div>	
	</div>
</section>

<?php $this->renderPartial('/layouts/_footer'); ?> 