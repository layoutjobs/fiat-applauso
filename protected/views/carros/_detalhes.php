<section id="veiculo-detalhes">
	<div class="row-fluid">
		<div class="span7">
			<div class="gallery">		
				<ul id="carro-detalhes-gallery">
					<li><?php echo CHtml::image($model->getUrlImgDestaque()); ?></li>
					<?php foreach($model->getUrlsImgs() as $urlImg) : ?>
						<?php echo $urlImg !== $model->getUrlImgDestaque() ? '<li>' . CHtml::image($urlImg) . '</li>' : ''; ?>
					<?php endforeach; ?>
				</ul>				
			</div>
		</div>
		<div class="span5">
			<br />
			<p><strong><span class="nome"><?php echo CHtml::encode($model->nome); ?></span><br />
			<span class="modelo"></span><?php echo CHtml::encode($model->modelo); ?></span><br />
			<span class="combustivel"><?php echo CHtml::encode($model->combustivel); ?></span></strong></p>
			<table>
				<tbody>
					<?php if ($model->qtdePortas) : ?>
					<tr>
						<th>Portas: </th>
						<td><?php echo CHtml::encode($model->getQtdePortas()); ?></td>
					</tr>
					<?php endif; ?>
					<?php if ($model->cor) : ?>
					<tr>
						<th>Cor:</th>
						<td><?php echo CHtml::encode($model->cor); ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<th>Marca:</th>
						<td><?php echo CHtml::encode($model->marca); ?></td>
					</tr>
					<tr>
						<th>Ano:</th>
						<td><?php echo CHtml::encode($model->anoModelo); ?> / <?php echo CHtml::encode($model->anoModelo); ?></td>
					</tr>
					<?php if ($model->getKmRodado()) : ?>
					<tr>
						<th>Km:</th>
						<td><?php echo CHtml::encode($model->getKmRodado()); ?></td>	
					</tr>
					<?php endif; ?>
				</tbody>
			</table>
		
			<span class="valor"><?php echo CHtml::encode($model->getValor('BRL', 'Consulte-nos')); ?></span>
			
			<?php if ($model->promocao == true) : ?>
			
			<span class="label">Promoção</span>
			
			<?php endif; ?>
			
			<br /><br />
			
			<?php if ($model->acessorios) : ?>
			<strong>Acessórios:</strong><br />
			<span><?php echo CHtml::encode($model->acessorios); ?></span>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php Yii::app()->clientScript->registerPackage('galleryView');	?>
<?php Yii::app()->clientScript->registerPackage('prettyPhoto');	?>
<?php Yii::app()->clientScript->registerScript('veiculo-detalhes', '
$(\'#carro-detalhes-gallery\').galleryView({
	panel_width: 320,
	panel_height: 260,
	panel_scale: \'fit\',
	frame_height: 70,
	frame_height: 55,
	frame_scale: \'fit\',
	show_filmstrip_nav: false,
	show_infobar: false
});'); ?>