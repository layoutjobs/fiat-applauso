<?php
	$this->pageTitle=Yii::app()->name.' - Carros/ Novos Pronta-entrega';
	$this->banner    = array(
		array('image' => Yii::app()->baseUrl . '/images/banners/carros-pronta-entrega.jpg'),
	);
?>

<div class="row">
	<div class="span7">
		<article class="entry">
			<header class="page-header">
				<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
				<?php $this->renderPartial('/_boxes/compartilhar'); ?>
			</header>
			<section class="page-content">
				<p>Carros nacionais e importados com garantia e revisados.<br /> 
				Veja a listagem completa dos veículos a pronta-entrega na Applàuso<br />
				Veículos, é só escolher e vir buscá-lo. Fale com nossos consultores<br />
				de vendas.</p>
				
				<a class="button button-help" href="<?php echo $this->createUrl('/empresa/equipe-vendas'); ?>">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/icon-help-54.png'); ?>
					Compre agora!
					<span>Fale agora com um de nossos vendedores.</span>
				</a>	
			</section>
		</article><!-- entry -->
	</div>
	<div class="span5">
		<?php $this->renderPartial('../_boxes/carrosBuscaForm', array('model' => $model, 'parentView' => 'prontaEntrega')); ?>
	</div>
</div>

<?php if (isset($_GET['id'])) : ?>
	
<h5 class="title">Detalhes do veículo</h5>
<div class="row">
	<div class="span8">
		<?php $this->renderPartial('_detalhes', array('model'=>$model)); ?>	
	</div>
	<div class="span4">
		<?php $this->renderPartial('_testDriveForm', array('model'=>$testDriveForm)); ?>			
	</div>
</div>

<strong><?php echo CHtml::link('&lt Voltar a listagem de veículos', array('prontaentrega')); ?></strong>

<?php else : ?>
	
<?php $this->widget('bootstrap.widgets.BootGridView',array(
	'id'=>'veiculo-grid',
	'template'=>'{items}{pager}',
	'dataProvider'=>$dataProvider,
	'enableSorting'=>false,
	'pagerCssClass'=>'pagination pagination-centered',
	'columns'=>array(
		array(
			'type'=>'html',
			'header'=>'Modelos',
			'headerHtmlOptions'=>array('class'=>'img'),
			'htmlOptions'=>array('class'=>'img'),
			'value'=>'CHTML::image($data->getUrlImgDestaque(\'thumbnails\'))',
		),
		array(
			'type'=>'html',
			'headerHtmlOptions'=>array('class'=>'nome'),
			'htmlOptions'=>array('class'=>'nome'),
			'value'=>'CHtml::link($data->nome, array("prontaentrega","id"=>$data->id))."<br />".$data->modelo."<br />".$data->combustivel',
		),
		array(
			'name'=>'portas',
			'type'=>'html',
			'headerHtmlOptions'=>array('class'=>'portas'),
			'htmlOptions'=>array('class'=>'portas'),
			'value'=>'"<span>".$data->getQtdePortas()."</span>"',
		),
		array(
			'name'=>'cor',
			'type'=>'html',
			'headerHtmlOptions'=>array('class'=>'cor'),
			'htmlOptions'=>array('class'=>'cor'),
			'value'=>'"<span>".$data->cor."</span>"',
		),
		array(
			'name'=>'marca',
			'type'=>'html',
			'headerHtmlOptions'=>array('class'=>'marca'),
			'htmlOptions'=>array('class'=>'marca'),
			'value'=>'"<span>".$data->marca."</span>"',
		),
		array(
			'type'=>'html',
			'header'=>'Ano/Modelo',
			'headerHtmlOptions'=>array('class'=>'ano'),
			'htmlOptions'=>array('class'=>'ano'),
			'value'=>'"<span>".$data->anoLancto."/".$data->anoModelo."</span>"',
		),
		array(
			'name'=>'acessorios',
			'type'=>'html',
			'headerHtmlOptions'=>array('class'=>'acessorios'),
			'htmlOptions'=>array('class'=>'acessorios'),
			'value'=>'"<span>".$data->getAcessorios(130)."</span>"',
		),
		array(
			'name'=>'valor',
			'type'=>'html',
			'headerHtmlOptions'=>array('class'=>'valor'),
			'htmlOptions'=>array('class'=>'valor'),
			'value'=>'\'<span>\'.$data->getValor(\'\',\'Consulte-nos\').\'</span>\'.($data->promocao==true?\'<span class="label">Promoção</div>\':\'\')',
		),
	),
)); ?>

<?php Yii::app()->clientScript->registerScript('veiculo-grid', "
$('#veiculo-grid table tbody tr').click(function(){
	url = $(this).children('td.nome').children('a').attr('href');
	$(window.document.location).attr('href',url);
});
"); ?>

<?php endif; ?>