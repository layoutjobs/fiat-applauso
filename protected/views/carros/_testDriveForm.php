<section class="box test-drive-form">
	<h2 class="box-title">Teste-Drive</h2>
	<p>É muito importante conhecer o carro antes de comprar, agende agora o seu 
	teste drive. Preencha abaixo as informações que entreremos em contato.</p>
	<div class="form">
		<?php $form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
			'id' => 'test-drive-form',
		)); ?>	
		<?php echo $form->errorSummary($model, ''); ?>	
		<?php echo $form->textFieldRow($model, 'nome', array('placeholder' => 'Nome')); ?>
		<?php echo $form->textFieldRow($model, 'tel', array('placeholder' => 'Tel/celular')); ?>
		<?php echo $form->textFieldRow($model, 'email', array('placeholder' => 'E-mail')); ?>
		<?php echo $form->hiddenField($model, 'veiculo', array('placeholder' => 'Veículo')); ?>
		<div><?php echo CHtml::htmlButton('OK', array('type' => 'submit')); ?></div>
		<?php $this->endWidget(); ?>	
	</div>			
</section>