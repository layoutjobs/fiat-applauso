<?php $this->pageTitle = Yii::app()->name . ' - Erro'; ?>

<section id="error">
	<div class="container">
		<div class="alert alert-error">
			<strong>Erro <?php echo $code; ?>: </strong><?php echo CHtml::encode($message); ?>
		</div>
	</div>
</section>