<div class="span6">
	<div class="item">
		<time><?php echo str_replace('/', '.', CHtml::encode($data->data)); ?></time>
		<h5 class="title"><?php echo CHtml::encode($data->titulo); ?></h5>
		<?php if ($data->conteudo) : ?>
			<p>
				<?php echo CHtml::encode($data->getResumoConteudo(60)); ?>...
				<?php echo CHtml::link('Leia mais +', $this->createUrl('/empresa/noticias', array('id' => $data->id))); ?>
			</p>
		<?php endif; ?>
	</div>
</div>
