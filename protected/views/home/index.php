<?php 
$this->pageTitle = Yii::app()->name . ' - Bem-vindo'; 

for ($i = 1; $i <= 3; ++$i) {
	if (is_file(Yii::app()->basePath . '/../images/banners/home-index-' . $i . '.png'))
	$banners[] = array('image' => Yii::app()->baseUrl . '/images/banners/home-index-' . $i . '.png');
}
//$banner = array($banners);
?>

<section id="banner">
	<div class="container">
		<?php $this->widget('bootstrap.widgets.BootCarousel', array(
		    'items' => $banners,
		    'displayPrevAndNext' => (count($banners) > 1) ? true : false,
		    'htmlOptions' => array('class' => 'slide')
		)); ?>
		<div class="carros-novos-list">
			<h2 class="title">Carros 0km a pronta-entrega</h2>
			<div class="items">
				<?php foreach ($carrosNovosModel as $counter => $item) : ?>	
				<?php if ($item->imgDestaque != null && $counter < 15) : ?>		
				<div class="item">
					<a href="<?php echo $this->createUrl('/carros/pronta-entrega', array('marca' => $item->marca, 'modelo' => $item->nome)); ?>">
						<?php echo CHtml::image($item->getUrlImgDestaque()); ?>
						<h5 class="title"><?php echo CHtml::encode($item->nome); ?></h5>
					</a>
				</div>			
				<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<div class="span7">
			<?php $this->renderPartial('../_boxes/carrosPromocoesList', array('dataProvider' => $dataProvider)); ?>	
		</div>
		<div class="span5">
			<?php $this->renderPartial('../_boxes/carrosBuscaForm', array('model' => $model)); ?>	
		</div>
	</div>
	<div class="box banners">
		<div class="row">
			<div class="span">
				<a href="<?php echo Yii::app()->createUrl('/seguros/agoge'); ?>">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-agoge.png'); ?>
				</a>
			</div>
			<div class="span">
				<a href="<?php echo Yii::app()->createUrl('/servicos/assistencia-tecnica'); ?>">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-assistencia-tecnica.png'); ?>
				</a>
			</div>
			<div class="span">
				<a href="<?php echo Yii::app()->createUrl('/carros/pronta-entrega', array('marca' => 'fiat', 'modelo' => 'ducato')); ?>">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-ducato.png'); ?>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="span">
				<a href="<?php echo Yii::app()->createUrl('/servicos/fashion'); ?>">
					<?php echo CHtml::image(Yii::app()->baseUrl . '/images/box-fashion.png'); ?>
				</a>
			</div>
		</div>
	</div>	
	<?php $this->renderPartial('../_boxes/carrosNovosSlider', array('model' => $carrosNovosModel)); ?>
</div>

<?php
Yii::app()->clientScript->registerScript('home-index', '
$(\'.box-carros-promocoes-list img\').load(function(){
	$(\'.box-carros-busca-form\').height($(\'.box-carros-promocoes-list\').height());
});
');
?>