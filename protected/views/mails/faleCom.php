<p>Olá <?php echo CHtml::encode($model->funcionario->nome); ?>.</p>

<p>Você foi contactado através do formulário do site. Confira abaixo os detalhes.</p>

<table>
	<tbody>
		<tr>
			<th>Nome: </th>
			<td><?php echo $model->nome; ?></td>
		</tr>
		<tr>
			<th>Tel/celular: </th>
			<td><?php echo $model->tel; ?></td>
		</tr>
		<tr>
			<th>E-mail: </th>
			<td><?php echo $model->email; ?></td>
		</tr>
		<tr>
			<th>Mensagem: </th>
			<td><?php echo $model->mensagem; ?></td>
		</tr>
	</tbody>
</table>

<p><strong>Observação: </strong>A resposta desse e-mail irá para o remetente do formulário.</p>