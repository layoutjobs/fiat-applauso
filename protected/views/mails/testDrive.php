<?php
$placa = $model->veiculo->placa ? ' / Placa ' . $model->veiculo->placa : '';
$chassi = $model->veiculo->chassi ? ' / Chassi ' . $model->veiculo->chassi : '';
?>

<h2>Solicitação de Teste Drive</h2>

<p>Uma nova Solicitação de Teste Drive foi enviada através do website. Confira abaixo os detalhes.</p>

<table>
	<tbody>
		<tr>
			<th>Nome: </th>
			<td><?php echo $model->nome; ?></td>
		</tr>
		<tr>
			<th>Tel/celular: </th>
			<td><?php echo $model->tel; ?></td>
		</tr>
		<tr>
			<th>E-mail: </th>
			<td><?php echo $model->email; ?></td>
		</tr>
		<tr>
			<th>Veículo: </th>
			<td><?php echo $model->veiculo->nome; ?> <?php echo $placa; ?> <?php echo $chassi; ?></td>
		</tr>
	</tbody>
</table>

<p><strong>Observação: </strong>A resposta desse e-mail irá para o solicitante.</p>