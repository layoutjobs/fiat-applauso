<div style="font: 13px/18px Arial, Helvetica, sans-serif;">
	<h2>Trabalhe Conosco</h2>
	<p>Um novo formulário de Trabalhe Conosco foi enviado através do website. Confira abaixo os detalhes.</p>	
	<table style="width: 100%; font: 13px/18px Arial, Helvetica, sans-serif;">
		<tbody>
			<tr><th colspan="2" style="border: 1px solid #CCC;">Dados Pessoais</th></tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('nome') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->nome); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('dataNascimento') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->dataNascimento); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('sexo') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->sexo); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('estadoCivil') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->estadoCivil); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('endereco') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->endereco); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('complemento') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->complemento); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('bairro') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->bairro); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('cidade') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->cidade); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('uf') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->uf); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('cep') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->cep); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('telefone') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->telefone); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('celular') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->celular); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('email') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->email); ?>
			</tr>
			<tr><th colspan="2" style="border: 1px solid #CCC;">Cargo Pretendido</th></tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('cargo') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->cargo); ?>
			</tr>
			<tr><th colspan="2" style="border: 1px solid #CCC;">Formação Acadêmica</th></tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('formAcadGrauInst') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->formAcadGrauInst); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('formAcadCurso') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->formAcadCurso); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('formAcadInstit') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->formAcadInstit); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('formAcadAnoConcl') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->formAcadAnoConcl); ?>
			</tr>
			<tr><th colspan="2" style="border: 1px solid #CCC;">Histórico Profissional</th></tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfEmpresa') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfEmpresa); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfDataAdmis') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfDataAdmis); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfDataDeslig') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfDataDeslig); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfFuncao') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfFuncao); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfDescAtiv') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfDescAtiv); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfEmpresa1') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfEmpresa); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfDataAdmis1') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfDataAdmis); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfDataDeslig1') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfDataDeslig); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfFuncao1') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfFuncao); ?>
			</tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('histProfDescAtiv1') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->histProfDescAtiv); ?>
			</tr>
			<tr><th colspan="2" style="border: 1px solid #CCC;">Qualificações</th></tr>
			<tr>
				<?php echo CHtml::tag('th', array('style' => 'text-align: left; vertical-align: middle'), $model->getAttributeLabel('qualificacoes') . ': '); ?>
				<?php echo CHtml::tag('td', array('style' => 'vertical-align: middle'), $model->qualificacoes); ?>
			</tr>
		</tbody>
	</table>
	<p><strong>Observação: </strong>A resposta deste e-mail irá para o e-mail da pessoa que enviou o formulário.</p>
</div>