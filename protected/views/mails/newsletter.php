<h2>Assinatura de Newsletter</h2>

<p>Uma nova Assinatura de Newsletter foi enviada através do website. Confira abaixo os detalhes.</p>

<table>
	<tbody>
		<tr>
			<th>Email: </th>
			<td><?php echo $email; ?></td>
		</tr>
	</tbody>
</table>

<p><strong>Observação: </strong>A resposta desse e-mail irá para o solicitante.</p>