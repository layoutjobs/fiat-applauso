<h2>Informação de Produto Fiat Fashion</h2>

<p>Uma nova solicitação de Informação do Produto <strong><?php echo $model->produto->nome; ?></strong> 
foi enviada através do site. Confira abaixo os detalhes do formulário.</p>

<table>
	<tbody>
		<tr>
			<th>Produto: </th>
			<td><?php echo $model->produto->nome; ?></td>
		</tr>
		<tr>
			<th>Nome: </th>
			<td><?php echo $model->nome; ?></td>
		</tr>
		<tr>
			<th>Tel/celular: </th>
			<td><?php echo $model->tel; ?></td>
		</tr>
		<tr>
			<th>E-mail: </th>
			<td><?php echo $model->email; ?></td>
		</tr>
		<tr>
			<th>Mensagem: </th>
			<td><?php echo $model->mensagem; ?></td>
		</tr>
	</tbody>
</table>

<p><strong>Observação: </strong>A resposta desse e-mail irá para o remetente do formulário.</p>