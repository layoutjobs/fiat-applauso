<h2>Solicitação de Revisão</h2>

<p>Uma nova Solicitação de Revisão foi enviada através do website. Confira abaixo os detalhes.</p>

<table>
	<tbody>
		<tr>
			<th>Nome: </th>
			<td><?php echo $model->nome; ?></td>
		</tr>
		<tr>
			<th>Telefone: </th>
			<td><?php echo $model->tel; ?></td>
		</tr>
		<tr>
			<th>E-mail: </th>
			<td><?php echo $model->email; ?></td>
		</tr>
		<tr>
			<th>Veículo: </th>
			<td><?php echo $model->veiculo; ?></td>
		</tr>
	</tbody>
</table>

<p><strong>Observação: </strong>A resposta desse e-mail irá para o solicitante.</p>